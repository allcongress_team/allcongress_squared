module.exports = {
  browserSync: {
    hostname: "http://allcongress.drude",
    port: 3000,
    openAutomatically: true,
    reloadDelay: 50,
    injectChanges: true,
  },

  drush: {
    enabled: true,
    alias: {
      css_js: 'drush cc css-js',
      cr: 'drush cc all'
    }
  },

  tpl: {
    rebuildOnChange: true
  }
};
