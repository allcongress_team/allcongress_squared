
    <nav class="pushy pushy-left">

            <ul>

                    <li class="pushy-link top"><a class="first" href="/"><span></span><span class="text">Home</span></a></li>
                    <li class="pushy-link"><a href="/#why-allcongress"><span class="icon-ac-allcongress-webfont-92"></span><span class="text">Why Allcongress?</span></a></li>
                    <li class="pushy-link"><a href="/promote-event"><span></span><span class="text">Promote Your Event</span></a></li>
                    <hr>


                    <li class="pushy-link"><a href="/"><div class="register-btn">Add your  medical event</div></a></li>



                    <hr>
                    <li class="pushy-link bottom"><a href="/">Sign in</a></li>
                    <li class="pushy-link top"><a href="/">Register</a></li>



            </ul>

    </nav>







<!-- Site Overlay -->
<div class="site-overlay"></div>

<!-- Content -->
<div id="main-container">



    <!-- Mobile version top menu -->


    <header class="header container-fluid visible-sm visible-xs">


      <nav class="header__top-menu">











        <span class="col-xs-2" >
          <!-- Menu Button xs-->
          <div class="menu-btn visible-xs">&#9776;</div>

          <!-- Menu Button sm-->
          <div class="menu-btn visible-sm">&#9776; Menu</div>

        </span>

        <!-- <a href="/" title="All Congress" class="logo-img"><img alt="All Congress" src="<?php echo base_path().path_to_theme(); ?>/images/logo-white.png"/></a> -->
        <div class="col-xs-8">
          <div class="header__top-menu__logo">
                  <a href="/" title="AllCongress">
                    <span class="logo-text"><span>ALL</span>CONGRESS</span>
                  </a>
          </div>
        </div>

        <div class="header__top-menu__secondary-menu pull-right">
          <div class="clearfix">

              <div class="language-selection">
                <?php
                  $lang_dropdown = lang_dropdown_block_view();
                  echo render($lang_dropdown['content']);
                 ?>
              </div>
            </div>

        </div>



      </nav>


    </header>






<header class="header container-fluid push">





<nav class="header__top-menu visible-lg visible-md">
        <div class="header__top-menu__logo">
                <a href="/" title="AllCongress">
                  <span class="logo-text"><span>ALL</span>CONGRESS</span>
                </a>
        </div>
        <ul class="header__top-menu__large-menu">
                <li><a href="/#why-allcongress"><span class="text">WHY ALLCONGRESS?</span></a></li>


                <li><a href="/promote-event"><span class="text">PROMOTE YOUR EVENT</span></a></li>


        </ul>
      <div class="header__top-menu__secondary-menu pull-right  visible-md visible-lg">
        <div class="clearfix">
            <div class="second-menu">
              <?php if (!empty($secondary_nav)): ?>
                <?php echo render($secondary_nav); ?>
              <?php endif; ?>
            </div>
            <div class="language-selection">
              <?php
                $lang_dropdown = lang_dropdown_block_view();
                echo render($lang_dropdown['content']);
               ?>
            </div>
          </div>
           <div class="clearfix"><a href="/"><div class="register-btn">ADD YOUR MEDICAL EVENT</div></a></div>
      </div>






</nav>

<!-- <div class="search-bar visible-sm visible-xs">

    <div class="clearfix">
      <div class="search-input-wrapper">
        <input id="main-search" class="main-search" type="search" required>
      </div>
      <a href="#" class="advanced-search-toggle"><span>Advanced search</span> <i class="fa fa-angle-right"></i></a>
    </div>

</div> -->

<?php if (!empty($site_slogan)): ?>
  <p class="lead"><?php echo $site_slogan; ?></p>
<?php endif; ?>

<?php echo render($page['header']); ?>

</header><!-- end header -->
