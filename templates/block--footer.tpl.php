
<footer class="page-footer">
  <div class="container">



   <div class="row hidden-xs">
    <div class="footer-group1">
      <ul class="col-md-15 col-sm-15 col-lg-15">
        <li><h3>HELP</h3></li>
        <li><a href="#!">Feedback  </a></li>
        <li><a href="/faq"> FAQ  </a></li>
        <li><a href="#!">Contact  </a></li>
        <li><a href="#!">Video Tutorials   </a></li>
       </ul>
       <ul class="col-md-15 col-sm-15 col-lg-15">
        <li><h3>PROMOTE</h3></li>
        <li><a href="/promote-event"> Event Promotion  </a></li>
        <li><a href="#!">Advertise with us  </a></li>
       </ul>
       <ul class="col-md-15 col-sm-15 col-lg-15">
        <li><h3>SERVICES</h3></li>
        <li><a href="#!">Pricing for PCOs  </a></li>
        <li><a href="#!">Online registration  </a></li>
        <li><a href="#!"> Organizers </a></li>
      </ul>
      <ul class="col-md-15 col-sm-15 col-lg-15">
        <li><h3>EVENTS</h3></li>
        <li><a href="#!">Most Popular  </a></li>
        <li><a href="#!">Most Visited  </a></li>
      </ul>
      <ul class="col-md-15 col-sm-15 col-lg-15">
        <li><h3>SOCIAL</h3></li>
        <li><a class="social-link" href="/"><svg class="icon ac-allcongress-90" role="img"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ac-allcongress-90"></use>
</svg><span class="social-text">Like Us</span></a></li>
        <li><a class="social-link" href="/"><svg class="icon ac-allcongress-80" role="img"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ac-allcongress-80"></use>
</svg><span class="social-text">Follow Us</span></a></li>
        <li><a class="social-link" href="/"><svg class="icon ac-allcongress-64" role="img"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ac-allcongress-64"></use>
</svg><span class="social-text">Add Us</span></a></li>
        <li><a class="social-link" href="/"><svg class="icon ac-allcongress-67" role="img"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ac-allcongress-67"></use>
</svg><span class="social-text">Befriend Us</span></a></li>
        <li><a class="social-link" href="/"><svg class="icon ac-allcongress-76" role="img"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ac-allcongress-76"></use>
</svg><span class="social-text">Watch Us</span></a></li>
      </ul>

    </div>
</div>

    <div class="row visible-xs visited-sm">
      <div class="mobile-footer-group1 col-xs-12">
        <a class="col-xs-4" href="#!">Feedback  </a>
        <a class="col-xs-4" href="/faq"> FAQ  </a>
        <a class="col-xs-4" href="#!">Contact  </a>
        <a class="col-xs-6" href="#!">Video Tutorials   </a>
        <a class="col-xs-6" href="/promote-event"> Event Promotion  </a>
        <a class="col-xs-6" href="#!">Advertise with us  </a>
        <a class="col-xs-6" href="#!">Pricing for PCOs  </a>
        <a class="col-xs-6" href="#!">Online registration  </a>
        <a class="col-xs-6" href="#!"> Organizers </a>
      </div>
    </div>

    <div class="row">
      <div class="mobile-footer-group2 col-xs-12">
        <div class="separator">
        <a class="col-sm-1 col-xs-3" href="/home">Home</a>
        <a class="col-sm-1 col-xs-3" href="#!">PCOs</a>
        <a class="col-sm-1 col-xs-3" href="#!">Venues</a>
        <a class="col-sm-1 col-xs-3" href="#!">News</a>
        <a class="col-sm-1 col-xs-4" href="#!">Terms</a>
        <a class="col-sm-1 col-xs-4"col-xs-3 href="#!">Privacy</a>
        <a class="col-sm-1 col-xs-4" href="#!">Cookies</a>
        <div class="register-btn col-sm-5 col-xs-12"><a href="#!">Add your medical event</a></div>
        </div>
        </div>
    </div>
    <div class="row visible-xs visited-sm">
      <div class="social-icons col-xs-12">
          <div class="separator">
            <li><a class="social-link" href="/"><svg class="icon ac-allcongress-90" role="img"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ac-allcongress-90"></use>
    </svg><span class="social-text">Like Us</span></a></li>
            <li><a class="social-link" href="/"><svg class="icon ac-allcongress-80" role="img"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ac-allcongress-80"></use>
    </svg><span class="social-text">Follow Us</span></a></li>
            <li><a class="social-link" href="/"><svg class="icon ac-allcongress-64" role="img"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ac-allcongress-64"></use>
    </svg><span class="social-text">Add Us</span></a></li>
            <li><a class="social-link" href="/"><svg class="icon ac-allcongress-67" role="img"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ac-allcongress-67"></use>
    </svg><span class="social-text">Befriend Us</span></a></li>
            <li><a class="social-link" href="/"><svg class="icon ac-allcongress-76" role="img"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ac-allcongress-76"></use>
    </svg><span class="social-text">Watch Us</span></a></li>
          </div>
      </div>
    </div>



    <div class="row footer-copyright">
      <a href="#main-container" class="scroll totop" style="display: block;">to top</a>
      <div class="logo-text"><span>ALL</span>CONGRESS</div>
      <div class="rights">© All rights reserved 2016-2017</div>
    </div>

  </div>

</footer>

  </div>
