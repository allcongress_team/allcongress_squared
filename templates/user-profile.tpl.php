<?php

/**
 * @file
 * Default theme implementation to present all user profile data.
 *
 * This template is used when viewing a registered member's profile page,
 * e.g., example.com/user/123. 123 being the users ID.
 *
 * Use render($user_profile) to print all profile items, or print a subset
 * such as render($user_profile['user_picture']). Always call
 * render($user_profile) at the end in order to print all remaining items. If
 * the item is a category, it will contain all its profile items. By default,
 * $user_profile['summary'] is provided, which contains data on the user's
 * history. Other data can be included by modules. $user_profile['user_picture']
 * is available for showing the account picture.
 *
 * Available variables:
 *   - $user_profile: An array of profile items. Use render() to print them.
 *   - Field variables: for each field instance attached to the user a
 *     corresponding variable is defined; e.g., $account->field_example has a
 *     variable $field_example defined. When needing to access a field's raw
 *     values, developers/themers are strongly encouraged to use these
 *     variables. Otherwise they will have to explicitly specify the desired
 *     field language, e.g. $account->field_example['en'], thus overriding any
 *     language negotiation rule that was previously applied.
 *
 * @see user-profile-category.tpl.php
 *   Where the html is handled for the group.
 * @see user-profile-item.tpl.php
 *   Where the html is handled for each item in the group.
 * @see template_preprocess_user_profile()
 *
 * @ingroup themeable
 */
$isPCO=false;


$rows = array();
$events = views_get_view_result('venue_events', 'block');

$active='';
$active_count=0;
$expired='';
$expired_count=0;
foreach($events as $event){

	$nation=$event->field_field_nation[0]['raw']['safe_value'];
//	print $event.'</br>';
	$nid=$event->nid;
	$title=$event->node_title;
	$date=field_get_items("node", $event->_field_data['nid']['entity'], "field_event_date_to");
	$date=$date[0]['value'];

	$city=field_get_items("node", $event->_field_data['nid']['entity'], "field_event_city");
	$city=$city[0]['value'];


	date( 'd-m-Y',$timestamp = strtotime($date) );

	if($timestamp >time()) {
		$active_count++;
		$active .= "<div class='venue-events-wrapper'>
				<div class='event-status'>
				Expired
				</div>
				<div class='details'>
					<div class='detail-wrapper'>
						<div class='info'>
						" . $city . " , " . $nation . " | " . $timestamp . "
						</div>
						<div class='title'>
							" . $title . "
						</div>
					</div>
				</div>
			</div>";
	}
	else{
		$expired_count++;
		$expired .= "<div class='venue-events-wrapper'>
				<div class='event-status'>
				Expired
				</div>
				<div class='details'>
					<div class='detail-wrapper'>
						<div class='info'>
						" . $city . " , " . $nation . " | " . $timestamp . "
						</div>
						<div class='title'>
							" . $title . "
						</div>
					</div>
				</div>
			</div>";
	}
}




$object=$user_profile['field_first_name']['#object'];
$nation=$object->field_nation['und'][0]['country']->name;
$description=$object->field_description['und'][0]['value'];
$phone=$object->field_company_phone['und'][0]['value'];
$city=$object->field_company_city['und'][0]['value'];
$email=$object->field_company_email['und'][0]['value'];
$website=$object->field_company_website['und'][0]['value'];
$address=$object->field_company_address['und'][0]['value'];


	/*check if user is CPO */
	foreach($user_profile['field_first_name']['#object']->roles as $role) {
		if (strpos($role, 'PCO') !== false) {
			$isPCO=true;
			break;
		}
	}

	if(!$isPCO){ ?>
		<div class="profile"<?php print $attributes; ?>
		<?php print render($user_profile); }
	else { ?>
<!--			<div class="profile"--><?php //print $attributes; ?>
<!--				--><?php //print render($user_profile); } ?>
			<div class="container">
				<div class="row pco">
					<div class="col-md-6 PCO-logo">
						<img src="https://pbs.twimg.com/profile_images/1435678469/MedicSkills_Logo_RGB_JPG_SQUARE.JPG"
						     class="img-responsive" />
					</div>
					<div class="col-md-6">
						<div class="type">EVENT ORGANIZER</div>
						<h1 class="title">
							<?php
								print $user_profile['field_user_nicename']['#items'][0]['value'];
							?>
						</h1>
						<div class="description">
							<?php print $description; ?>
							Professional Congress Organisers will organise for you a medical event form A to Z.
							Below you can see all the medical events organised by this PCO and also contact details from his buisness card.
						</div>
						<div class="details row">
							<div class="col-md-12">
								<div class="address">
									<i class="fa fa-map-marker fa-lg"></i>
									<?php print $address.','.$nation ?>

								</div>
								<div class="phone">
									<i class="fa fa-phone fa-lg"></i>
									<?php print $phone; ?>
								</div>
								<div class="email">
									<i class="fa fa-envelope fa-lg"></i>
									<a href="">
										<?php print $email ?>
									</a>
								</div>
								<div class="website">
									<i class="fa fa-globe fa-lg"></i>
									<a href="http//:<?php print $website ?>">
									<?php print $website ?>
									</a>
								</div>
							</div>

						</div>
					</div>
				</div>

	<div class="associated-events" >
		<div class="container">
			<div class="paddings-wrapper row">
				<div class="col-md-8">

					<span class="active-events">
						<span class="text">
							<span class="number"><?php print $active_count ?></span>
							Events Active
						</span>
					</span>

					<span class="expired-events">
						<span class="text">
							<span class="number"><?php print $expired_count ?></span>
							Events Expired
						</span>
					</span>


				<div class="row">
					<div class="col-md-12">

						<div class="view-wrapper active">
							<?php
	//						print views_embed_view('venue_events', 'default');
							print $active;
							?>
						</div>
						<div class="view-wrapper expired">
							<?php
							//						print views_embed_view('venue_events', 'default');
							print $expired;
							?>
						</div>
					</div>
				</div>



				</div>
				<div class="col-md-4 search">
					<div class="search-panel">
						<span class=" search-tab" >
							Search for venues <i class="fa fa-search"></i>
						</span>
						<div class="search-content">
							<div class="btn btn-primary ">
								SEARCH
							</div>
						</div>
					</div>

					<div class="button-wrapper row">
						<div class="col-xs-2 ">
							<img src="<?php print base_path() . path_to_theme(); ?>/images/venues-buildings.png" class="button-img">
						</div>
						<div class="col-xs-10 text">
							FIND VENUES AND EXHBITIONS CENTERS
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>


<div class='section-map events-section' id="event-map">
	<div class="container">
		<div class="section-title-wrapper ">
			<h3 class='section-title background-color-white section-title-triangle-grey '>MAP</h3>
		</div>
	</div>
	<?php
//		$coordinates = field_get_items("node", $node, "field_coordinates");
//		$coordinates = $coordinates[0]['value'];
		$coordinates= '10.0, 10.0';
	?>
	<div id="events-map" class="events-map" data-coordinates="<?php print $coordinates; ?>"></div>
</div>
			</div>
	<?php } ?>
