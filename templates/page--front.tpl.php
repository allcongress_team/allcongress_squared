<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>


<!-- insert header block tpl as per https://coderwall.com/p/g1q-yg/drupal-simple-universal-header-footer-include -->

<?php include './'. path_to_theme() .'/templates/block--header.tpl.php';?>





<main class="content">


<section class="map container-fluid">
  <div class="map-section">
      <div class="map-canvas-wrapper"></div>

      <div class="map-cta">
          <div class="container">
              <h2 class="text-center"> <span>SEARCH & BOOK</span> <br> A MEDICAL CONFERENCE IN A FLASH</h2>
              <div class="visible-sm visible-xs row" id="search-popup"><div class="tap">tap for advanced search</div><button type="button" class="btn btn-warning">SEARCH</button></div>
          </div>
      </div>

      <div class="search-by-specialty-dropdown">
          <div class="specialties container">
              <form>
                  <div class="row">
  				<h3 class="title"><?php print t('CHOOSE SPECIALTY')?></h3>
                      <?php
                                  $vocabulary = taxonomy_vocabulary_machine_name_load('specialty');
                                  $vid = $vocabulary->vid;
                                  $specialties = taxonomy_get_tree($vid);
                                  foreach ($specialties as $value) {
                                      print '<div class="dropd-item col-md-3"><label class="">'.$value->name.' <input type="checkbox" name="'.$value->name.'" value="'.$value->tid.'"/></label></div>';
                                  }
                               ?>
                  </div>
              </form>
              <div class="form-actions-wrapper clearfix">

                  <div class="btn ok-btn">OK</div>
                  <div class="btn close-btn">CANCEL</div>
              </div>
          </div>
      </div>
  	<div class="search-dropdown-content dropd-type hidden">
                                  <div class="clearfix">
  									<h3 class="title"><?php print t('CHOOSE TYPE')?></h3>
                                      <form>
                                          <div class="row">
                                              <?php
                                              $vocabulary = taxonomy_vocabulary_machine_name_load('congress_type');
                                              $vid = $vocabulary->vid;
                                              $specialties = taxonomy_get_tree($vid);
                                              foreach ($specialties as $value) {
                                                  print '<div class="dropd-item col-md-4"><label> '.$value->name.' <input type="checkbox" name="'.$value->name.'" value="'.$value->tid.'"/></label></div>';
                                              }
                                           ?>
                                          </div>
                                      </form>
                                      <div class="form-actions-wrapper clearfix">

  										<div class="btn ok-btn">OK</div>
  										<div class="btn close-btn">CANCEL</div>
                                      </div>
                                  </div>
      </div>
  	<div class="search-dropdown-content dropd-location hidden">
                                  <div class="clearfix">
  									<h3 class="title"><?php print t('LOCATION')?></h3>
                                      <form>
                                          <input type="text" placeholder="Search City" value="" name="city" class="location-city" />
                                          <input type="text" placeholder="Search Country" value="" name="country" class="location-country" />
                                      </form>
                                      <div class="form-actions-wrapper clearfix">
  										<div class="btn ok-btn">OK</div>
  										<div class="btn close-btn">CANCEL</div>
                                      </div>
                                  </div>
      </div>
  	 <div class="search-dropdown-content dropd-date hidden">
                                  <div class="clearfix">
  									<h3 class="title"><?php print t('DATE')?></h3>
                                      <form>
                                          <div class="date-input">
                                              <input type="text" placeholder="From" value="" name="date-from" class="search-date-from" />
                                              <span class="fa fa-calendar"></span>
                                          </div>
                                          <div class="date-input">
                                              <input type="text" placeholder="To" value="" name="date-to" class="search-date-to" />
                                              <span class="fa fa-calendar"></span>
                                          </div>
                                      </form>
                                      <div class="form-actions-wrapper clearfix">
                                          <div class="btn ok-btn">OK</div>
  										<div class="btn close-btn">CANCEL</div>
                                      </div>
                                  </div>
       </div>



   <div class="main-search-wrapper clearfix container visible-md visible-lg">
     <div class="row">
      <div class="main-search-container">

              <div class="main-search-inner">
                  <div class="main-search-inner-content clearfix">
  					 <div class="search-by-specialty-wrapper">
  						<div class="search-by-specialty-inner clearfix">
  							<div class="text-wrapper">
  								<!-- <div class="small-text">
  									<?php print t('Search by')?>
  								</div> -->
  								<!-- <div class="large-text"> -->
  									<?php print t('Specialty')?>
  							   <!--  </div> -->
  								<i class="fa fa-angle-right pull-right"></i>
  							</div>

  						</div>
  					</div>
  					<div class="tagit-wrapper clearfix">
                          <ul class="tagit"></ul>
                      </div>
                      <div class="search-by-lists">
                          <div class="search-by-type search-by dropd-wrapper visible-md visible-lg">
                              <div class="search-dropdown dropd" data-target=".dropd-type">Type</div>

                          </div>
                          <div class="search-by-location search-by dropd-wrapper visible-md visible-lg">
                              <div class="search-dropdown dropd" data-target=".dropd-location">Location</div>

                          </div>
                          <div class="search-by-date search-by dropd-wrapper visible-md visible-lg ">
                              <div class="search-dropdown dropd" data-target=".dropd-date">Date <svg class="icon ac-allcongress-101" role="img">
                    <use xlink:href="#ac-allcongress-101"></use>
                  <svg></div>
                          </div>
                      </div>
                      <div class="map-search-submit">
                          <input type="submit" value="<?php print t('Search')?>"></div>
                          <svg class="icon ac-allcongress-94" role="img">
                <use xlink:href="#ac-allcongress-94"></use>
              <svg>


                  </div>
              </div>
          </div>
  	 </div>







  </div>

  <div class="map-search-results-wrapper">
      <div class="container">
          <div class="clearfix">
              <span class="search-results-title">SEARCH RESULTS</span>
              <span class="search-results-filters"></span>
          </div>
          <div clas="clearfix">
              <div class="row">
                  <div class="map-search-results-inner">
                      <div class="content clearfix"></div>
                      <div class="see-more"><a class="btn btn-block search-results-see-more">SEE MORE</a></div>
                  </div>
              </div>
          </div>
      </div>
  </div>

  </section>



<section class="container">

      <div class="featured-events-wrapper">
       <h2><?php print t('Featured Events')?></h2>
          <div class="text-center front-cta visible-sm visible-md visible-lg">

            <svg class="icon ac-allcongress-186" role="img">
  <use xlink:href="#ac-allcongress-186"></use>
<svg>
            <span class="icon-ac-allcongress-webfont-137"></span><a class="btn-link">PROMOTE YOUR EVENT</a>

          </div>


          </br>


      	<?php print views_embed_view('featured_events'); ?>

        <div class="text-center front-cta visible-xs">

          <svg class="icon ac-allcongress-186" role="img">
<use xlink:href="#ac-allcongress-186"></use>
<svg>
          <span class="icon-ac-allcongress-webfont-137"></span><a class="btn-link">PROMOTE YOUR EVENT</a>

        </div>

      </div>

</section>





<section class="add-event-venue-section-wrapper">
    <div class="add-event-venue-section container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                        <div class="texts">
                            <h3>ARE YOU A VENUE OR PCO?</h3>
                            <div class="subtitle">ADD YOUR EVENT & VENUE NOW </div>
                            <a href="#" class="add-link"><svg class="icon ac-allcongress-85" role="img">
                  <use xlink:href="#ac-allcongress-85"></use>
                <svg><div class=text>ADD YOUR EVENT</div></a>
                            <a href="#" class="add-link"><svg class="icon ac-allcongress-107" role="img">
                  <use xlink:href="#ac-allcongress-107"></use>
                <svg><div class=text>ADD YOUR VENUE</div></a>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</section>



<section class="container-fluid">
      <div class="row">
      <?php echo render($page['content']); ?>
      </div>
</section>




</main>


<!-- insert footer block tpl as per https://coderwall.com/p/g1q-yg/drupal-simple-universal-header-footer-include -->

<?php include './'. path_to_theme() .'/templates/block--footer.tpl.php';?>




<script>
var donought_stats1Data = [
        {
          value : 500-203,
          color : "#99ccff"
        },
        {
          value: 203,
          color:"#FFF"
        }
      ]

var donought_stats2Data = [
              {
                value: 500-353,
                color:"#99ccff"
              },
              {
                value : 353,
                color : "#FFF"
              }
            ]

var donought_stats3Data = [
                          {
                            value: 500-103,
                            color:"#99ccff"
                          },
                          {
                            value : 103,
                            color : "#FFF"
                          }
                        ]




var inView = false;

function isScrolledIntoView(elem)
{
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemTop <= docViewBottom) && (elemBottom >= docViewTop));
}

$(window).scroll(function() {
    if (isScrolledIntoView('#donought_stats1')) {
        if (inView) { return; }
        inView = true;
        new  Chart(document.getElementById("donought_stats1").getContext("2d")).Doughnut(donought_stats1Data,{showTooltips: false,
          segmentShowStroke : false,responsive: true,percentageInnerCutout : 85});
        new Chart(document.getElementById("donought_stats2").getContext("2d")).Doughnut(donought_stats2Data,{showTooltips: false,
          segmentShowStroke : false,responsive: true,percentageInnerCutout : 85});

        new Chart(document.getElementById("donought_stats3").getContext("2d")).Doughnut(donought_stats3Data,{showTooltips: false,
          segmentShowStroke : false,responsive: true,percentageInnerCutout : 85});

    } else {
        inView = false;
    }
});

</script>
