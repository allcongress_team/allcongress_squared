<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */


/**
*$venue = field_get_items("node", $node, "field_venue");
* if ($venue) {
*    if(isset($venue[0]['entity']->title))
*	    $hotel_name = $venue[0]['entity']->title;
*    if(field_get_items("node", $node, "field_webpage_link"))
*	    $link = field_get_items("node", $node, "field_webpage_link");
*    if(isset($venue[0]['entity']->field_coordinates['und'][0]['value']))
*	    $venue_coord=$venue[0]['entity']->field_coordinates['und'][0]['value'];
*}
**/

?>






<!-- insert header block tpl as per https://coderwall.com/p/g1q-yg/drupal-simple-universal-header-footer-include -->

<?php include './'. path_to_theme() .'/templates/block--header.tpl.php';?>




<main class="content  <?php //print $container_class ?>">





  <section class="container-fluid">
        <div class="row">
      <!--  <?php if(arg(2)!='edit'){ ?>-->


        <?php print $messages; ?>
        <?php echo render($page['content']); ?>
        <?php $new_view=views_get_view_result('event_header', 'default');
      //  dpm($new_view);
    //    $export = kprint_r($new_view, TRUE); // $vars - is a variable which you want to print.
  //print $export;
   ?>
        </div>
  </section>



<section class="container-fluid event-header">




    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <h1 class="event-header__title">
                    <?php print $node->title; ?>
                    <?php print $related['venue']->title; ?>
                    <?php foreach ($new_view as $item => $value) {
                        print 'at ';
                      print $value->node_field_data_field_venue__field_data_field_address_field_;
                    } ?>

                    <?php foreach ($new_view as $item => $value) {
                      print ', ';
                      print $value->field_field_event_date_from_1[0]['rendered'] ['#markup'];
                    } ?>



                </h1>

            </div>
            <div class="col-sm-3 pull-right">

                    <div class="event-header__register">
                      <div class="row">
                        <div class="col-md-12">
                            <div class="event-header__days-left">
                                <?php
                                $start = field_get_items("node", $node, "field_event_date_from");
                                if ($start) {
                                    $now = time(); // or your date as well
                                    $start = strtotime($start[0]['value']);
                                    $datediff = abs($now - $start);
                                    print floor($datediff / (60 * 60 * 24));
                                }
                                ?>
                            </div>
                            DAYS UNTIL THE EVENT
                        </div>
                       </div>
                       <div class="row">
                        <div class="col-md-12 event-header__booking relative">
                            <a href="">
                                <div class="book-title btn btn-default"><svg class="icon ac-allcongress-53" role="img">
                  <use xlink:href="#ac-allcongress-53"></use>
                <svg><span>Book</span></div>
                            </a>
                            <div class="text"> YOUR TICKETS <strong>NOW</strong></div>
                        </div>
                      </div>
                    </div>

            </div>
        </div>
    </div>
</section>












<div id="events-navbar" class="events-navbar">
	<nav class="navbar navbar-default ">
		<div class="container">
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"  >
				<ul class="nav navbar-nav" id="events-submenu">
<!--					<li><a data-href="event-preview">Event Prewiew</a></li>-->
					<li><a data-href="event-general-info">General Info</a></li>
					<li><a data-href="event-overview">Event Overview</a></li>
					<li><a data-href="event-registration">Registration</a></li>
					<li><a data-href="event-scientific-content">Scientific Content</a></li>
					<li><a data-href="event-scientific-content">Accomodation</a></li>
					<li><a data-href="event-sponsors-exhibitor">Sponsors Exhibitors</a></li>
					<li><a data-href="event-map">Map</a></li>
					<li><a data-href="event-contact">Contact</a></li>
					<li><a data-href="event-relevant-events">Relevant Events</a></li>
					<li><a data-href="event-arrow"><svg class="icon ac-allcongress-132" role="img">
<use xlink:href="#ac-allcongress-132"></use>
<svg></a></li>

				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
</div>



<div class="events-info-wrapper" data-spy="scroll" data-target="#events-submenu" data-offset="0">
	<div class="container event " id="event-preview">
	    <div class="row">


	        <div class="col-md-8 general-info" id="event-general-info">
	            <div class="title">GENERAL INFO</div>




                <div class="row synopsis">
                    <div class="col-md-3">
                  <?php foreach ($new_view as $item => $value) {

              //      print_r ( $value->_field_data["nid"]['entity']->field_featured_image );
                    $featured_image=$value->_field_data["nid"]['entity']->field_featured_image;
                //   $image_url=file_create_url($featured_image['und'][0]['uri']);
                  //print $image_url;


                    $image_style_url=image_style_url('featured_image',$featured_image['und'][0]['uri']);





                  //  $hero_image = array(
  //'style_name' => '',
  //'path' => $featured_image['uri'],
  //'width' => '',
  //'height' => '',
  //'alt' => $featured_image['alt'],
  //'title' => $featured_image['title'],
  //);
//print theme('image_style',$hero_image);


                  } ?>


        <!--  change to real poster image        <img class="poster_image" src="<?php print $image_style_url;?>" class="img-responsive"> -->

                  <img class="poster_image" src="/<?php print drupal_get_path('theme', 'allcongress_squared'); ?>/assets/images/event_final.jpg" class="img-responsive">

                    </div>
                  <div class="col-md-9">
                  <span class="synopsis_specialty">
                    <p><strong>Speciality: </strong>
                    <?php print $new_view[0]->field_field_main_specialty[0]['rendered']['#title'];

                   ?>, <?php print $new_view[0]->field_field_secondary_specialty[0]['rendered']['#title']?>

</p>

                  </span>
                  <span class="synopsis_type">
                  <p><strong>Type: </strong> <?php print $new_view[0]->field_field_types[0]['rendered']['#title']?></p></span>

                  <span class="synopsis_fees"><p><strong>Fees: </strong> <?php print $new_view[0]->field_field_reg_cost_from[0]['rendered']['#markup']?>  - <?php print $new_view[0]->field_field_reg_cost_to[0]['rendered']['#markup']?></p></span>
                  <span class="synopsis_dates">
                  <p><strong>Dates: </strong> <?php print $new_view[0]->field_field_event_date_from_1[0]['rendered']['#markup']?>  - <?php print $new_view[0]->field_field_event_date_to[0]['rendered']['#markup']?></p></span>


                </div>


                </div>

                <div class="hotel-info">
              <p><strong>Venue:</strong> <?php foreach ($new_view as $item => $value) {
                print $value->node_field_data_field_venue_title;
              } ?></p>






	            <p class="hotel-location">
                <?php foreach ($new_view as $item => $value) {
                  print $value->node_field_data_field_venue__field_data_field_address_field_;
                  print ', ';
                  print $value->node_field_data_field_venue__field_data_field_address_field__1;
                } ?>
              </p> <a class="venue-more" href="#"> <div class="text">
	                    More about the venue
	                </div> </a>
	            </div>




        <div class="hotel-owl-wrapper">
				  <div id="sync1" class="owl-carousel">
					<div class="item"><img src="https://upload.wikimedia.org/wikipedia/commons/e/e3/Dushanbe_2010_01_Hotel_Tajikistan.jpg" class="img-responsive"/></div>
					<div class="item"><img src="http://www.libertyhotelslara.com/dosyalar/resimler/liberty-lara-hotel1.jpg" class="img-responsive" /></div>
					<div class="item"><img src="https://upload.wikimedia.org/wikipedia/commons/9/94/Resort_Hotel_Olivean_Shodoshima_Japan05n.jpg" class="img-responsive"/></div>
				  </div>

				  <div id="sync2" class="owl-carousel">
					<div class="item owl-item-margin"><img src="https://upload.wikimedia.org/wikipedia/commons/e/e3/Dushanbe_2010_01_Hotel_Tajikistan.jpg" class="img-responsive"/></div>
					<div class="item owl-item-margin"><img src="http://www.libertyhotelslara.com/dosyalar/resimler/liberty-lara-hotel1.jpg" class="img-responsive"/></div>
					<div class="item owl-item-margin"><img src="https://upload.wikimedia.org/wikipedia/commons/9/94/Resort_Hotel_Olivean_Shodoshima_Japan05n.jpg" class="img-responsive"/></div>
				  </div>
				</div>




        <div class='subsection venue-description'>


            <div class='accompanying-text'>
                <p>The Scottish Exhibition and Conference Centre (SECC) is the perfect location for any event. It's superbly situated in Glasgow, Scotland's largest and most exciting city. The Centre provides a wide range of conference and exhibition facilities along with a 4 star hotel - all under one roof.</p>
            </div>
        </div>


        <div class='subsection weather'>



                <p><strong>Weather (monthly averages)</strong></p>

                <div class="weather_ico">
                  <span class="ico"><img src="http://www.allcongress.com/content/themes/allcongress/images/weather_ico.png"></span>
                  <span class="month">Jun</span>
                </div>

                <span class="temp_title">Max</span>
                <span class="temp_val"><span id="max_temp">15.64</span>C / <span id="max_temp_f">60.15</span>F</span>

                <span class="temp_title">Min</span>
                <span class="temp_val"><span id="min_temp">10.71</span>C / <span id="min_temp_f">51.28</span></span>
                <span class="weather_text">The above data in our Weather Info table are temperature predictions for the date of the medical event for Glasgow, Uk.</span>

          
        </div>

        <div class='subsection welcome-message'>


            <div class='accompanying-text'>
                <p><strong>Welcome message</strong></p>
                <p>The Scottish Exhibition and Conference Centre (SECC) is the perfect location for any event. It's superbly situated in Glasgow, Scotland's largest and most exciting city. The Centre provides a wide range of conference and exhibition facilities along with a 4 star hotel - all under one roof.</p>
            </div>
        </div>


          <!--
              <div class="subtitle">Transportation Guide</div>

	            <div class="transportation-link-wrapper">
	                <a class="transportation-link" data-toggle="collapse" href="#collapseAir" aria-expanded="false"
	                   aria-controls="collapseAir">
	                    Air Travel
	                    <i class="fa fa-chevron-down pull-right"></i>
	                </a>

	                <div class="collapse" id="collapseAir">
	                    orbi in ligula et neque imperdiet egestas. Nullam sit amet tempor sem, a consequat diam.
	                    Donec efficitur rutrum metus vitae consequat. Suspendisse fringilla tincidunt ligula, et tincidunt
	                    lectus fermentum non.
	                </div>
	            </div>

	            <div class="transportation-link-wrapper">
	                <a class="transportation-link" data-toggle="collapse" href="#collapseSkybus" aria-expanded="false"
	                   aria-controls="collapseSkybus">
	                    Skybus
	                    <i class="fa fa-chevron-down pull-right"></i>
	                </a>

	                <div class="collapse" id="collapseSkybus">
	                    orbi in ligula et neque imperdiet egestas. Nullam sit amet tempor sem, a consequat diam.
	                    Donec efficitur rutrum metus vitae consequat. Suspendisse fringilla tincidunt ligula, et tincidunt
	                    lectus fermentum non.
	                </div>
	            </div>

	            <div class="transportation-link-wrapper">
	                <a class="transportation-link" data-toggle="collapse" href="#collapseTaxi" aria-expanded="false"
	                   aria-controls="collapseTaxi">
	                    Taxi
	                    <i class="fa fa-chevron-down pull-right"></i>
	                </a>

	                <div class="collapse" id="collapseTaxi">
	                    orbi in ligula et neque imperdiet egestas. Nullam sit amet tempor sem, a consequat diam.
	                    Donec efficitur rutrum metus vitae consequat. Suspendisse fringilla tincidunt ligula, et tincidunt
	                    lectus fermentum non.
	                </div>
	            </div>

	            <div class="transportation-link-wrapper">
	                <a class="transportation-link" data-toggle="collapse" href="#collapsePublicTransport"
	                   aria-expanded="false" aria-controls="collapsePublicTransport">
	                    Public Transport
	                    <i class="fa fa-chevron-down pull-right"></i>
	                </a>

	                <div class="collapse" id="collapsePublicTransport">
	                    orbi in ligula et neque imperdiet egestas. Nullam sit amet tempor sem, a consequat diam.
	                    Donec efficitur rutrum metus vitae consequat. Suspendisse fringilla tincidunt ligula, et tincidunt
	                    lectus fermentum non.
	                </div>
	            </div>


	            <div class="transportation-link-wrapper">
	                <a class="transportation-link" data-toggle="collapse" href="#collapseBus" aria-expanded="false"
	                   aria-controls="#collapseBus">
	                    Melbourne Visitor Shuttle Bus
	                    <i class="fa fa-chevron-down pull-right"></i>
	                </a>

	                <div class="collapse" id="collapseBus">
	                    orbi in ligula et neque imperdiet egestas. Nullam sit amet tempor sem, a consequat diam.
	                    Donec efficitur rutrum metus vitae consequat. Suspendisse fringilla tincidunt ligula, et tincidunt
	                    lectus fermentum non.
	                </div>
	            </div>-->



	        </div>

	        <div class="col-md-4 fst-col visible-md visible-lg">
				<img src="<?php print base_path() . path_to_theme(); ?>/assets/images/medtech.jpg" class="img-responsive"/>
				<div class="special event-panel">
	                <div class="title">Special Event</div>
	                <div class="event-panel-body">
	                    <div class="panel-button"><svg class="icon ac-allcongress-162" role="img">
            <use xlink:href="#ac-allcongress-162"></use>
          </svg><a>Scientific Program</a></div>
	                    <div class="panel-button"><svg class="icon ac-allcongress-167" role="img">
            <use xlink:href="#ac-allcongress-167"></use>
          </svg><a>Sponsorship & Exhibition Consentious</a></div>
	                    <div class="panel-button"><svg class="icon ac-allcongress-174" role="img">
            <use xlink:href="#ac-allcongress-174"></use>
          </svg><a>Venue's Floor Plans</a></div>
	                </div>
	            </div>
	            <div class="event-panel">
	                <div class="title">Event Tools</div>
	                <div class="event-panel-body">
	                    <div class="panel-button"><svg class="icon ac-allcongress-70" role="img">
            <use xlink:href="#ac-allcongress-70"></use>
          </svg>Add to Favorites</div>
	                    <div class="panel-button"><svg class="icon ac-allcongress-170" role="img">
            <use xlink:href="#ac-allcongress-170"></use>
          </svg>Notify me by email</div>
	                    <div class="panel-button"><svg class="icon ac-allcongress-48" role="img">
            <use xlink:href="#ac-allcongress-48"></use>
          </svg>Share this Event</div>
	                    <div class="panel-button"><svg class="icon ac-allcongress-92" role="img">
            <use xlink:href="#ac-allcongress-92"></use>
          </svg>Print this Event</div>
	                    <div class="panel-button"><svg class="icon ac-allcongress-168" role="img">
            <use xlink:href="#ac-allcongress-168"></use>
          </svg>Claim this event</div>
	                </div>
	            </div>
	            <!-- <div class="social-links">
	                <div class="title"><i class="fa fa-users"></i>&nbsp;&nbsp;Social Media</div>
	                <div class="event-panel-body">
	                    <div class="panel-button"><i class="fa fa-facebook-official"></i>&nbsp;&nbsp;Share on Facebook</div>
	                    <div class="panel-button"><i class="fa fa-linkedin-square"></i>&nbsp;&nbsp;Share on LinkedIn</div>
	                    <div class="panel-button"><i class="fa fa-twitter-square"></i>&nbsp;&nbsp;Share on Twitter</div>
	                    <div class="panel-button"><i class="fa fa-google-plus-square"></i>&nbsp;&nbsp;Share on Google+</div>
	                </div>
	            </div> -->
	            <div class="event-panel">
	                <div class="title">Add to my Calendar</div>
	                <div class="event-panel-body">
	                    <div class="panel-button"><svg class="icon ac-allcongress-101" role="img">
            <use xlink:href="#ac-allcongress-101"></use>
          </svg>Internet Calendar format
	                    </div>
	                    <div class="panel-button"><svg class="icon ac-allcongress-100" role="img">
            <use xlink:href="#ac-allcongress-100"></use>
          </svg>Google Calendar format</div>
	                    <div class="panel-button"><svg class="icon ac-allcongress-112" role="img">
            <use xlink:href="#ac-allcongress-112"></use>
          </svg>Add it to your smartphone
	                    </div>
	                </div>
				    <img class="qr-code"
						src="https://chart.googleapis.com/chart?chs=150x150&amp;cht=qr&amp;chl=http://all-congress.wxyz.gr/node/<?php print $node->nid; ?>&amp;choe=UTF-8"
						alt="QR code">
	            </div>

				<div class="reservation">



	            <div class="hotel-reservation">Hotel Reservation</div>
	            </br>
                <?php $country = field_get_items("node", $node, "field_nation"); ?>
                <div class="booking-widget">
                    <iframe src="https://www.booking.com/general.html?aid=359520&tmpl=searchbox&width=260&bgcolor=f5f5f5&ss=<?php print $city[0]['value'].','.$country[0]['safe']; ?>" height="255" width="285" frameborder="0" scrolling="no" name="sbox_iframe"></iframe>
                </div>
				</div>
	            <div class="button-wrapper find-pcos">
	               <!--  <div class="col-xs-2 ">
	                    <img src="<?php print base_path() . path_to_theme(); ?>/images/professionals.png" class="button-img"/>
	                </div> -->
	                <a><div class="text">
	                    FIND PCOS
	                </div></a>
	            </div>
	            <div class="button-wrapper find-venues">
	                <!-- <div class="col-xs-2 ">
	                    <img src="<?php print base_path() . path_to_theme(); ?>/images/venues-buildings.png" class="button-img"/>
	                </div> -->
	                <a><div class="text">
	                    FIND VENUES
	                </div></a>
	            </div>


	        </div>


	    </div>
	</div>


	<div  class="section-overview section-allocongress background-color-grey" id="event-overview">
	    <div class="container">
	        <div class="section-title-wrapper ">
	            <h3 class='section-title background-color-white section-title-triangle-grey'>EVENT OVERVIEW</h3>
	        </div>
	        <div class='section-content'>
	            <?php $prologue = field_get_items("node", $node, "field_prologue");
	            if ($prologue)
	                print $prologue[0]['value'];
	            ?>
	            <?php $body = field_get_items("node", $node, "body");
	            if ($body)
	                print $body[0]['value'];

	            $roster = field_get_items("node", $node, "field_roster");
	            if ($roster)
	                print '</br>';
	            print $roster[0]['value'];

	            ?>
	        </div>


	    </div>
	</div>


	<!--





	section registration






	-->


	<div class="section-registration events-section" id="event-registration">
	    <div class="container">
	        <div class="section-title-wrapper ">
	            <h3 class='section-title  section-title-triangle-grey section-title-triangle-white'>
	                REGISTRATION <span class='section-link'><a> | up to date </a></span></h3>
	        </div>
	        <div class='section-content'>
	            <div class='subsection'>
	                <h4 class='section-subtitle'>General info </h4>

	                <div class='general-info-text '>
	                    <?php
	                    $prologue = field_get_items("node", $node, "field_prologue");
	                    if ($prologue)
	                        print $prologue[0]['value'];
	                    ?>
	                    <!--					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore-->
	                    <!--					et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut-->
	                    <!--					aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse-->
	                    <!--					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in-->
	                    <!--					culpa qui officia deserunt mollit anim id est laborum."-->
	                </div>
	            </div>

	            <div class='subsection'>
	                <h4 class='section-subtitle'>Registration Cost </h4>

	                <div class='registration-table-wrapper row'>
	                    <div class='col-md-10'>
	                        <table class="table ">
	                            <thead>
	                            <tr>
	                                <th>Type</th>
	                                <th>Early Registration </br> 2015-08-04</th>
	                                <th>Late Registration </br> From:2015-08-05</br>  To: 2015-10-05</th>
	                                <th>On site registration</th>
	                            </tr>
	                            </thead>
	                            <tbody>
	                            <tr>
	                                <td>Physicians and Scientists</td>
	                                <td>999</td>
	                                <td>999</td>
	                                <td>999</td>
	                            </tr>
	                            <tr>
	                                <td>Nurses, Students, Residents*</td>
	                                <td>999</td>
	                                <td>999</td>
	                                <td>999</td>
	                            </tr>
	                            </tbody>
	                        </table>
	                    </div>
	                </div>

	                <div class='registration-info-text'> All prices are in Australian dollars and include GST.</div>
	                <div class='text-italic'> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
	                    eiusmod tempor incididunt ut labore et dolore magna aliqua.
	                </div>
	            </div>

	            <div class='subsection accompanying'>
	                <h4 class='section-subtitle'>Accompanying Persons </h4>

	                <div class='accompanying-text'>Accompanying persons not allowed</div>
	            </div>

	            <div class='subsection registration-Documents'>
	                <h4 class='section-subtitle'>Registration Documents</h4>

	                <div class='accompanying-text'>
	                    <span class='text-bold'>Letter of invitation:</span> We don’t provide letter of invitation </br>
	                    <span class='text-bold'>Letter of confirmation:</span> We can provide upon request Letter of
	                    Confirmation </br>
	                    <span class='text-bold'>Proof of professional / educational status:</span> We don’t need Proof of
	                    professional/educational status </br>
	                    <span class='text-bold'>Online Abstract Submission:</span> We don’t accept Online Abstracts </br>
	                </div>
	            </div>

	            <div class='subsection registration-terms'>
	                <h4 class='section-subtitle'>Registration / Cancelation Terms and Conditions</h4>

	                <div class='accompanying-text'>
	                    <span class='text-bold'> Liability </span>
	                    </br>CoBRA cannot accept liability for personal accidents, nor loss of or damage to private property
	                    of participants, either during or directly arising from The World Congress on Controversies in
	                    Breast Cancer.
	                    </br>Participants should make their own arrangements with respect to health and travel insurance.
	                </div>
	                </br>
	                <div class="registration-button btn btn-deafult btn-lg">
	                    REGISTER NOW
	                </div>
	            </div>
	        </div>
	    </div>
	</div>


	<!--





	section scientific content






	-->

	<div class='section-scientific-content events-section' id="event-scientific-content">
	    <div class="container">
	        <div class="section-title-wrapper ">
	            <h3 class='section-title background-color-white section-title-triangle-grey '>
	                Scientific Content <span class='section-link'><a> | up to date </a></span></h3>
	        </div>
	        <div class="section-content">

	                        <div class="section-subtitle"> Abstract submission deadline</div>
	                        2015-07-06

	                        <div class="section-subtitle"></div>
	                        Editorial Guidelines for Abstract Submission

	                        Abstracts should be submitted in English and contain no more than 3,600 characters, including titles,
	                        graphs and non-visible characters, such as spaces and line breaks. </br>
	                        Please write concisely and clearly and follow this structure:
	                        <ul class="abstracts-list">
	                            <li> Problem statement: Why do we care about the problem?</li>
	                            <li> Methods: What did you actually do to get your results?</li>
	                            <li> Results: What did you learn/invent/create?</li>
	                            <li>Conclusion: What are the implications of your findings?</li>
	                        </ul>
	                        Please ensure that your abstract does not contain spelling, grammar or scientific mistakes, as it will
	                        be reproduced exactly as submitted. No proof reading will be done. Linguistic accuracy is the
	                        responsibility of the submitter.
	                        </br>
	                        </br>An abstract cannot be accepted if the conflict of interest disclosure is not signed.
	                        </br>Abstracts must contain data and meet international ethical standards.
	                        </br>Abbreviations should be defined.
	                        </br>
	                        Images/graphs/tables can be uploaded according to the following criteria:
	                        </br>
	            <!--			<ul class="images-list">-->
	            <!--				<li>Width (in pixels): min 50 / max 750</li>-->
	            <!--				<li>Height (in pixels): min 50 / max 750</li>-->
	            <!--				<li>Size (in KB): min 20/ max 1,000</li>-->
	            <!--				<li>Resolution (dpi): min 96 / max 300. The standard display size for images is 96 dpi.</li>-->
	            <!--				<li>Format: JPG, PNG or GIF</li>-->
	            <!--			</ul>-->
	            <!--			</br> You may improve the quality of your image for publication by increasing the number of dpi as well-->
	            <!--			as the height and width of the image (in pixels).-->
	            <!--			<div class='text-bold'>If you wish to modify your image, you may use the programme Paint.Net (free-->
	            <!--				software).-->
	            <!--			</div>-->
	            <!--			<div class='text-bold'>Abstract submissions that do not meet the above noted guidelines may be rejected-->
	            <!--				without review.-->
	            <!--			</div>-->
	            <!--			<div class='text-bold'>The Scientific Committee may request additional information about abstracts and-->
	            <!--				reject abstracts.-->
	            <!--			</div>-->
	            <!---->
	            <!---->
	            <!--			<div class="text-bold">Scientific Awards</div>-->
	            <!--			What is this competition all about? </br>-->
	            <!--			</br>-->
	            <!---->
	            <!--			</br>Controversies in Breast Cancer (CoBRA) invites young scientists-->
	            <!--			</br>or researchers to submit their work and to join the Young Scientist Award competition.-->
	            <!---->
	            <!--			The winner(s) will receive free registration, 3 nights’ hotel accommodation and the abstract will be-->
	            <!--			presented as an oral presentation as an oral presentation.-->
	            <!--			</br>-->
	            <!--			<div class="text-bold">Wow! How do I enter?</div>-->
	            <!--			<ul class="enter-list">-->
	            <!--				<li> Submit your abstract via the Congress website (Abstract) no later than Monday July 6, 2015.</li>-->
	            <!--				<li> Register and pay registration fees (If your abstract is selected as a winner, your full-->
	            <!--					registration fees will be refunded).-->
	            <!--				</li>-->
	            <!---->
	            <!--				<li>Include your date of birth during the registration process.</li>-->
	            <!--			</ul>-->
	            <!--			<div class="text-bold">Great! So what are the guidelines for submitting?</div>-->
	            <!--			<ul class="submitting-list">-->
	            <!--				<li>Submitters must be born in 1981 or after.</li>-->
	            <!--				<li>Submitters must be the first author of the abstract</li>-->
	            <!--				<li>Attendance at the Congress by the winner is mandatory in order to receive and collect the award</li>-->
	            <!--				<li>Winner(s) who cancel for any reason will not be entitled to collect their award.</li>-->
	            <!--			</ul>-->
	            <!--			<div class="text-italic">Note that abstracts sent by email or fax will automatically be rejected.</div>-->
	        </div>
	    </div>
	</div>

	<!--





	section sponsors






	-->
	<div class='section-sponsors events-section' id="event-sponsors-exhibitor">
	    <div class="container">
	        <div class="section-title-wrapper ">
	            <h3 class='section-title section-title-triangle-grey section-title-triangle-white'>
	                SPONSORS EXHIBITORS </h3>
	        </div>
	        <div class="section-content">
	            <div class="section-subtitle"> General Info</div>
	            <div class="">
	                <?php $info = field_get_items("node", $node, "field_exhibit_info");
	                if ($info)
	                    print $info[0]['value'];
	                ?>
	                <!--				<div class="col-md-10">-->
	                <!--					Supporting CoBRA - What's in it for the Industry?-->
	                <!--					<ul class="sponsors-list">-->
	                <!--						<li>Participate and present your products and research to an international audience </li>-->
	                <!--						<li>Stimulate and discuss new research</li>-->
	                <!--						<li>Use the unique debates concept to stir discussion</li>-->
	                <!--						<li>Encourage and promote education and public awareness</li>-->
	                <!--						<li>Cooperate and network with world opinion leaders in the field</li>-->
	                <!--						<li>Support and assist world opinion leaders to reach the best clinical recommendations for current therapeutic dilemmas</li>-->
	                <!--						<li>Inform and update international participants on clinical and technological developments</li>-->
	                <!--					</ul>-->
	                <!--				</div>-->
	                <!--				<div class="col-md-2">-->
	                <!--					dfgsdfsdfsd-->
	                <!--				</div>-->

		            <div class="section-subtitle"> Sponsors / Supporters</div>
		            </br>
		            <div class="sponsors-button btn btn-deafult btn-lg">
		                VIEW EXHIBITION CONSENSUS
		            </div>
		        </div>
		    </div>
		</div>
	</div>
		<!--





	section map






	-->

	<div class='section-map events-section' id="event-map">
	    <div class="container">
	        <div class="section-title-wrapper ">
	            <h3 class='section-title background-color-white section-title-triangle-grey '>MAP</h3>
	        </div>
	    </div>
        <div id="eventpage-map" class="events-map" data-coordinates="<?php print $new_view[0]->field_field_position[0]['raw']['lat'];

       ?>,<?php print $new_view[0]->field_field_position[0]['raw']['lon']?>"></div>


	</div>

	<!--





	section contact






	-->
	<div class='section-contact events-section' id="event-contact">
	    <div class="container">
	        <div class="section-title-wrapper ">
	            <h3 class='section-title section-title-triangle-grey section-title-triangle-white'>
	                CONTACT</h3>
	        </div>
	        <div class="section-content">
	            <?php $contact = field_get_items("node", $node, "field_contact_details");
	            if ($contact)
	                print $contact[0]['value'];
	            ?>
	        </div>

	    </div>
	</div>

	<!--





	section events






	-->
	<div class='section-relevant events events-section background-color-grey' id="event-relevant-events">
	    <div class="container">
	        <div class="section-title-wrapper ">
	            <h3 class='section-title background-color-white section-title-triangle-grey section-title-triangle-grey'>
	                RELEVANT EVENTS</h3>
	        </div>
	        <?php
	        print views_embed_view('relevant_events', 'default');
	        ?>

	    </div>
	</div>
<!--	--><?php
//	$block = block_load('views', 'events_relevant-block');
//	print render(_block_get_renderable_array(_block_render_blocks(array($block))));
//	?>
	<?php } ?>
</div>


<!--Print debug info
<pre>
  <?php print_r($new_view) ?>
</pre>
-->


</main>





<!-- insert footer block tpl as per https://coderwall.com/p/g1q-yg/drupal-simple-universal-header-footer-include -->

<?php include './'. path_to_theme() .'/templates/block--footer.tpl.php';?>
