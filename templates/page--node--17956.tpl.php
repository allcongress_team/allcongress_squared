<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>



<!-- insert header block tpl as per https://coderwall.com/p/g1q-yg/drupal-simple-universal-header-footer-include -->

<?php include './'. path_to_theme() .'/templates/block--header.tpl.php';?>







<main class="content  <?php //print $container_class ?>">




  <div class="container-fluid">

  <div class="row">



      <!-- /#promote-event-section -->
<div class="section-wrapper">
<div class="section-body"><div class="promote-event-section-wrapper">
    <div class="promote-event-section container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                        <div class="text-center">

                          <svg class="icon ac-allcongress-134" role="img">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ac-allcongress-134"></use>
                  <svg></svg></svg>

                            <h2><?php echo t('Promote your event'); ?></h2>
                            <div class="subtitle"><?php echo t('
Posting your event at allcongress.com is the best way to promote it worldwide.<br/> Increase your visibility by 42% per 6 months period & get advanced stats for your events.'); ?> </div>
                            <div class="col-sm-4 col-sm-offset-4 col-xs-12 col-centered">
				<div class="text-center front-stats">
					<a class="start-btn button-primary btn btn-default" href="#">
						Start now					</a>
				</div>
			</div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
</div>
</div>

  <!-- /#promote-deserve-section -->
<div class="section-wrapper">
<div class="section-body"><div class="promote-deserve-section-wrapper">
<div class="promote-deserve-section container">
  <div class="row">
      <div class="col-md-12">
          <div class="row">
                  <div class="text-center">

                      <h2><?php echo t('Your event deserves 1st class'); ?></h2>
                      <div class="subtitle"><?php echo t('Get your event to have the best view in Allcongress.<br/> Increase your  visibility by 42% per 6 months period &amp get advanced stats for your events'); ?> </div>

                  </div>
              </div>
          </div>
      </div>
</div>
</div>
</div>
</div>


<!-- /#promote-visibility-section -->
<div class="section-wrapper">
<div class="section-body"><div class="promote-visibility-section-wrapper">
<div class="promote-visibility-section container">
<div class="row">
    <div class="col-md-12">
        <div class="row">
                <div class="text-center">

                    <h2><?php echo t('Increase your visibility'); ?></h2>
                    <div class="subtitle"><?php echo t('Currently allcongress.com receives more than 18.000 visits per month with an increase rate of 7%.<br/>
Posting your event at allcongress.com is the best way to promote it worldwide.'); ?> </div>


                    <div class="col-md-6 center-block">
                <div class="row">
                        <div class="col-md-8  col-md-offset-2 text-center">
                          <svg class="icon ac-allcongress-3" role="img">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ac-allcongress-3"></use>
                  <svg></svg></svg>
                            <h3>Simple Listing</h3>
                            <div class="subtitle">Any given congress receives in a 6 month period approximately 2200 visits! </div>

                        </div>
                    </div>
                </div>


                <div class="col-md-6 center-block">
            <div class="row">
                    <div class="col-md-8  col-md-offset-2  text-center">
                      <svg class="icon ac-allcongress-20" role="img">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ac-allcongress-20"></use>
              <svg></svg></svg>
                        <h3>Promoted Listing</h3>
                        <div class="subtitle">If you decide to promote your event further, it will receive in a 6 month period approximately 4800 visits!</div>

                    </div>
                </div>
            </div>


                    <div class="col-sm-4 col-sm-offset-4 col-xs-12 col-centered">
<div class="text-center front-stats">
  <a class="start-btn button-primary btn btn-default" href="#">
    Promote your event now					</a>
</div>
</div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

<!-- /#promote-increase-section -->
<div class="section-wrapper">
<div class="section-body"><div class="promote-increase-section-wrapper">
<div class="promote-increase-section container">
<div class="row">
    <div class="col-md-12">
        <div class="row">
                <div class="text-center">

                    <h2><?php echo t('Increase attedance stats'); ?></h2>
                    <div class="subtitle"><?php echo t('Statistically in a 6 month period you can increase your event’s attendance by more than 15%.<br/>
Implementing Online Registration Service will increase the attendance by more than 32%'); ?> </div>


<div class="col-md-6 center-block">
<div class="row">
    <div class="col-md-10 text-center">
      <canvas id="buyers"></canvas>

    </div>
</div>
</div>
<div class="col-md-6 center-block visible-md visible-lg">
<div class="row">
    <div class="col-md-6  text-center">

      <canvas id="productChart1" width="170"></canvas>

      <div class="donut-inner">
        <h5>50%</h5>

      </div>

    </div>

    <div class="col-md-6 text-center">

      <canvas id="productChart2" width="170"></canvas>

      <div class="donut-inner">
        <h5>15%</h5>

      </div>

    </div>

</div>
</div>


<div class="col-sm-4 col-sm-offset-4 col-xs-12 col-centered">
<div class="text-center front-stats">
<a class="start-btn button-primary btn btn-default" href="#">
Promote your event now					</a>
</div>
</div>




</main>



<!-- insert footer block tpl as per https://coderwall.com/p/g1q-yg/drupal-simple-universal-header-footer-include -->

<?php include './'. path_to_theme() .'/templates/block--footer.tpl.php';?>




<script>
var pieData = [
        {
          value : 100-50,
          color : "#FFF"
        },
        {
          value: 50,
          color:"#777"
        }
      ]

var pieData2 = [
              {
                value: 15,
                color:"#FFF"
              },
              {
                value : 100-15,
                color : "#777"
              }
            ]

var buyerData = {
	labels : ["JAN","FEB","MAR","APR","MAY","JUN","JUL"],
	datasets : [
		{
			fillColor : "transparent",
			strokeColor : "#5DAAD5",
			pointColor : "#5DAAD5",
			pointStrokeColor : "transparent",
			data : [203,270,300,340,345,350,460]
		}
	]
}
var buyerOptions = {
  scaleShowGridLines : true,
  showTooltips: false,
  datasetStrokeWidth : 4,
  scaleFontColor: "#FFF",
  pointDotStrokeWidth : 7,
  scaleFontSize : 12,
  responsive: true,
}

var inView = false;

function isScrolledIntoView(elem)
{
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemTop <= docViewBottom) && (elemBottom >= docViewTop));
}

$(window).scroll(function() {
    if (isScrolledIntoView('#buyers')) {
        if (inView) { return; }
        inView = true;
        new Chart(document.getElementById("buyers").getContext("2d")).Line(buyerData, buyerOptions);

        new Chart(document.getElementById("productChart1").getContext("2d")).Doughnut(pieData,{showTooltips: false,
          segmentShowStroke : false,responsive: true,percentageInnerCutout : 85});

        new Chart(document.getElementById("productChart2").getContext("2d")).Doughnut(pieData2,{showTooltips: false,
          segmentShowStroke : false,responsive: true,percentageInnerCutout : 85});

    } else {
        inView = false;
    }
});





</script>
