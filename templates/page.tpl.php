<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>

<div class="visible-xs visible-sm">
    <nav id="my-menu">
      <div>
            <ul>
                <li class="menu-list-1 top"><a class="first" href="/"><span class="glyphicon glyphicon-home"></span><span class="text">HOME</span></a></li>
                <li class="menu-list-1"><a href="/"><span class="icon-ac-allcongress-webfont-92"></span><span class="text">WHY ALL CONGRESS</span></a></li>
                <li class="menu-list-1"><a href="/"><span class="icon-ac-allcongress-webfont-86"></span><span class="text">ORGANIZERS</span></a></li>
                <li class="menu-list-1 bottom"><a href="/"><span class="icon-ac-allcongress-webfont-172"></span><span class="text">EXHIBITION CENTERS</span></a></li>
                <li class="menu-list-2 top"><a href="/">HELP</a></li>
                <li class="menu-list-2 bottom"><a href="/">CONTACT</a></li>
                <li class="menu-list-3"><a href="/"><div class="register-btn">ADD YOUR MEDICAL EVENT</div></a></li>
                <li class="menu-list-4 top"><a href="/">REGISTER</a></li>
                <li class="menu-list-4 bottm"><a href="/">SIGN in</a></li>
            </ul>
      </div>
    </nav>
</div>

<div id="my-page">
<nav class="top">

  <div class="container-fluid top-menu visible-lg visible-md">
      <div class="row">
        <div class="logo">
                <a href="/" title="AllCongress">
                  <span class="logo-text"><span>ALL</span>CONGRESS</span>
                </a>
        </div>
        <ul class="large-menu">
                <li><a href="/"><span class="text">WHY ALL CONGRESS</span></a></li>
                <li><a href="/"><span class="text">ORGANIZERS</span></a></li>
                <li><a href="/"><span class="text">EXHIBITION CENTERS</span></a></li>
                <li><a href="/"><span class="text">PROMOTE YOUR EVENT</span></a></li>
                <li><a href="/">HELP</a></li>
                <li><a href="/">CONTACT</a></li>

        </ul>
      <div class="pull-right secondary-menu visible-md visible-lg">
        <div class="clearfix">
            <div class="second-menu">
              <?php if (!empty($secondary_nav)): ?>
                <?php print render($secondary_nav); ?>
              <?php endif; ?>
            </div>
            <div class="language-selection">
              <?php
                $lang_dropdown = lang_dropdown_block_view();
                print render($lang_dropdown['content']);
               ?>
            </div>
          </div>
           <div class="clearfix"><a href="/"><div class="register-btn">ADD YOUR MEDICAL EVENT</div></a></div>
      </div>

    </div>
  </div>

  <!-- Mobile version top menu -->
  <div class="container top-menu visible-sm visible-xs">
    <!-- <div class="pull-right secondary-menu visible-md visible-lg">
      <div class="social-icons">
      <a class="social-link" href="/"><span class="fa fa-facebook"></span></a>
      <a class="social-link" href="/"><span class="fa fa-twitter"></span></a>
      <a class="social-link" href="/"><span class="fa fa-linkedin"></span></a>
      <a class="social-link" href="/"><span class="fa fa-google-plus"></span></a>
      <a class="social-link" href="/"><span class="fa fa-youtube"></span></a>
      </div>
      <div class="clearfix">
      <?php if (!empty($secondary_nav)): ?>
        <?php print render($secondary_nav); ?>
      <?php endif; ?>
      </div>
    </div> -->



    <div class="row">
      <!-- The mm menu that appears in the mobile version -->

      <span class="mm-controllers visible-xs visible-sm col-xs-3" >
         <a href="#my-menu" id="my-button"><img src="/sites/default/files/burger_button.png"></a>
      </span>

      <!-- <a href="/" title="All Congress" class="logo-img"><img alt="All Congress" src="<?php print base_path() . path_to_theme(); ?>/images/logo-white.png"/></a> -->
      <div class="col-xs-6">
        <a href="/" title="All Congress">
          <span class="logo-text"><span>ALL</span>CONGRESS</span>
        </a>
      </div>
      <div class="language-selection col-xs-3">
        <?php
            $lang_dropdown = lang_dropdown_block_view();
            print render($lang_dropdown['content']);
        ?>
      </div>
    </div>
  </div>
</nav>

<header role="banner" id="page-header">
  <?php if (!empty($site_slogan)): ?>
    <p class="lead"><?php print $site_slogan; ?></p>
  <?php endif; ?>

  <?php print render($page['header']); ?>
</header> <!-- /#page-headers -->

<?php $container_class= ($is_front==TRUE) ? '' : 'container' ?>
<div class="main-container  <?php //print $container_class ?>">




  <div class="container-fluid">

  <div class="row">

    <?php if (!empty($page['sidebar_first'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_first']); ?>
      </aside>  <!-- /#sidebar-first -->
    <?php endif; ?>

    <section<?php print $content_column_class; ?>>
      <?php if (!empty($page['highlighted'])): ?>
        <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>
      <?php if (!empty($breadcrumb)): print $breadcrumb; endif;?>
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      <?php if (!empty($title)): ?>
        <h1 class="page-header"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php print $messages; ?>
      <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
      <?php endif; ?>
      <?php if (!empty($page['help'])): ?>
        <?php print render($page['help']); ?>
      <?php endif; ?>
      <?php if (!empty($action_links)): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
    </section>

    <?php if (!empty($page['sidebar_second'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_second']); ?>
      </aside>  <!-- /#sidebar-second -->
    <?php endif; ?>

    </div>
  </div>
</div>





<!-- insert footer block tpl as per https://coderwall.com/p/g1q-yg/drupal-simple-universal-header-footer-include -->

<?php include './'. path_to_theme() .'/templates/block--footer.tpl.php';?>
