var EventsMap = function(options) {

    var self = this;
    self.markers = [];
    self.infowindows = [];
    self.el = options.el;
    self.wrapper = options.wrapper;
    self.onBoundsChange = options.onBoundsChange;
    // Initialize
    this.init = function(target, cb) {

        var mapOptions = {
            // How zoomed in you want the map to start at (always required)
            zoom: 11,

            // The latitude and longitude to center the map (always required)
            center:     new google.maps.LatLng(40.6700, -73.9400), // New York

            // How you would like to style the map.
            // This is where you would paste any style found on Snazzy Maps.
            styles: [{
                "featureType": "landscape",
                "elementType": "labels",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "transit",
                "elementType": "labels",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "poi",
                "elementType": "labels",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "water",
                "elementType": "labels",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "stylers": [{
                    "hue": "#00aaff"
                }, {
                    "saturation": -100
                }, {
                    "gamma": 2.15
                }, {
                    "lightness": 12
                }]
            }, {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "lightness": 24
                }]
            }, {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [{
                    "lightness": 57
                }]
            }],
            scrollwheel: false
        };



        // Get the HTML DOM element that will contain your map
        // We are using a div with id="map" seen below in the <body>
        var mapElement = (target) ? $(target)[0] : $('.map-canvas-wrapper')[0];

        // Create the Google Map using our element and options defined above
        self.map = new google.maps.Map(mapElement, mapOptions);
        // OverlappingMarkerSpiderfier
        self.oms = new OverlappingMarkerSpiderfier(self.map);

        // bindinds
        this.bindMapBoundChange();

        if (cb) cb(self.map);
        return this;
    };

    this.showLoading = function() {
        if ($(self.wrapper).find('.map-loading').length === 0) {
            $(self.wrapper).append('<div class="map-loading"><i class="fa fa-spinner fa-spin"></i></div>');
        }
    };

    this.hideLoading = function() {
        $(self.wrapper).find('.map-loading').remove();
    };

    this.zoomOut = function(zoomLevel) {
        self.map.setZoom(3);
    };

    this.bindMapBoundChange = function() {
        self.map.addListener('bounds_changed', function() {
            if (self.onBoundsChange) self.onBoundsChange(self.map);
        });
    };

    this.fetchEvents = function(options, cb) {
        options = options || {};
        self.showLoading();
        var query = self.formatQuery(options.query || []);

        console.log('query: ', query);

        var events = [];
        $.get('/api/search-events-json' + query, function(data) {
            events = data.nodes.map(function(item) {
                ev = $.extend({
                    title: '',
                    latitude: '',
                    longitude: '',
                    city: '',
                    coordinates: '',
                    html: ''
                }, item.node);

                ev.latitude = parseFloat(ev.coordinates.split(',')[0]);
                ev.longitude = parseFloat(ev.coordinates.split(',')[1]);

                return ev;
            });
            console.log('events', events);
            if (options.addToMap) {
                self.clearMarkers();
                self.addMarkers(events);
            }

            self.hideLoading();
            if (cb) cb(events);
        });
    };
    //min and max limits for multiplier, for random numbers
    //keep the range pretty small, so markers are kept close by


    this.createMarker = function(latlng, text) {
        var min = 0.999999;
        var max = 1.000001;
        ///get array of markers currently in cluster
        var allMarkers = self.markers;

        //final position for marker, could be updated if another marker already exists in same position
        var finalLatLng = latlng;

        //check to see if any of the existing markers match the latlng of the new marker
        if (allMarkers.length !== 0) {
            for (i = 0; i < allMarkers.length; i++) {
                var existingMarker = allMarkers[i];
                var pos = existingMarker.getPosition();

                //if a marker already exists in the same position as this marker
                if (latlng.equals(pos)) {
                    //update the position of the coincident marker by applying a small multipler to its coordinates
                    var newLat = latlng.lat() + (Math.random() - 0.5) / 1500; // * (Math.random() * (max - min) + min);
                    var newLng = latlng.lng() + (Math.random() - 0.5) / 1500; // * (Math.random() * (max - min) + min);
                    finalLatLng = new google.maps.LatLng(newLat, newLng);
                }
            }
        }

        var marker = new google.maps.Marker({
            position: finalLatLng
        });

        // google.maps.event.addListener(marker, 'click', function() {
        //     infowindow.close();
        //     infowindow.setContent();
        //     infowindow.open(map, marker);
        // });

        return marker;
    };

    this.getPixelFromLatLng = function(latLng) {
        var projection = this.map.getProjection();
        //refer to the google.maps.Projection object in the Maps API reference
        var point = projection.fromLatLngToPoint(latLng);
        return point;
    };

    this.getInfowindowOffset = function(marker) {

        console.log('getInfowindowOffset', marker, marker.position);

        var quadrant = '';
        var offset;
        var center = self.map.getCenter();
        console.log('map.center', center, center.lat(), center.lng());
        var map = self.map;
        var point = self.getPixelFromLatLng(marker.position);
        console.log('point', point);

        quadrant += (point.y > center.lng()) ? "b" : "t";
        quadrant += (point.x < center.lat()) ? "l" : "r";

        console.log('quadrant', quadrant);

        if (quadrant == "tr") {
            offset = new google.maps.Size(-70, 185);
        } else if (quadrant == "tl") {
            offset = new google.maps.Size(70, 185);
        } else if (quadrant == "br") {
            offset = new google.maps.Size(-70, 20);
        } else if (quadrant == "bl") {
            offset = new google.maps.Size(70, 20);
        }
        console.log('offset', offset);
        //these values are subject to change based on map canvas size, infowindow size
        return offset;
    };

    this.addMarkers = function(events, cb) {
        var bounds = new google.maps.LatLngBounds();
        if (events) {

            if (!self.markerCluster) {
                var mIcon = '/sites/all/themes/allcongress_squared/assets/images/blue-marker.png';
                var mcOptions = {
                    minimumClusterSize: 6,
                    styles: [{
                        height: 32,
                        textColor: '#000',
                        url: mIcon,
                        width: 24,
                        anchor: [32, 24],
                        /*text position*/
                        backgroundPosition: '0 2px',
                        backgroundRepeat: 'no-repeat'
                    }]
                };

                self.markerCluster = new MarkerClusterer(self.map, [], mcOptions);
            }


            events.forEach(function(ev) {

                var latlng = new google.maps.LatLng(ev.latitude, ev.longitude);
                var icon = '/sites/all/themes/allcongress_squared/assets/images/blue-marker.png';

                var marker = new google.maps.Marker({
                    position: latlng,
                    map: self.map,
                    // animation: google.maps.Animation.DROP,
                    title: ev.title,
                    icon: icon,
                });
                //
                if (parseFloat(ev.latitude) == 47.602640 && parseFloat(ev.longitude) == -122.338371) {
                    console.log('same event', ev);
                }
                // var marker = self.createMarker(latlng, ev.title);
                //extend the bounds to include each marker's position
                bounds.extend(marker.position);
                self.markers.push(marker);
                self.oms.addMarker(marker);

                self.markerCluster.addMarker(marker);

                // Add infowindow
                var infowindow = new google.maps.InfoWindow({
                    content: '<div><a href="' + ev.path + '/edit">' + ev.title + '</a></div>'
                });

                self.infowindows.push(infowindow);

                // Marker click event...
                google.maps.event.addListener(marker, 'mouseover', function() {
                    self.infowindows.forEach(function(iw) {
                        iw.close();
                    });

                    infowindow.setOptions({
                        // disableAutoPan : true,
                        // pixelOffset:self.getInfowindowOffset(marker)
                    });

                    infowindow.open(self.map, marker);
                });

                google.maps.event.addListener(marker, 'mouseout', function() {
                    self.infowindows.forEach(function(iw) {
                        iw.close();
                    });
                });
                // var iw = new gm.InfoWindow();
                self.oms.addListener('click', function(marker, event) {
                    console.log('click', marker);
                    // iw.setContent(marker.desc);
                    // iw.open(map, marker);
                });
            });
        }

        // self.map.fitBounds(bounds);
        self.map.setZoom(3);

        if (cb) cb();
    };

    this.clearMarkers = function() {

        if (this.markers) {
            this.markers.forEach(function(marker) {
                marker.setMap(null);
            });
        }

        if (this.infowindows) {
            this.infowindows.forEach(function(iw) {
                iw.close();
            });
        }

        this.markers = [];
    };

    self.formatQuery = function(queryData) {
        var path = '';
        Object.keys(queryData).forEach(function(name, data) {
            var obj = queryData[name];
            var mapped = obj.map(function(item) {
                return name + '[]=' + item.value;
            }).join('&');
            path += mapped;
        });
        console.log(path);
        if (path !== '') path = '?' + path;
        return path;
    };

};




/**
 * Specialty form in Events map.
 */
var SpecialtyForm = function(options) {

    this.el = options.el;
    this.button = options.button;
    this.form = $(this.el).find('form');
    this.onSubmit = options.onSubmit;
    this.onChange = options.onChange;
    var self = this;
    this.bindEvents = function() {

        var self = this;

        // Toggle dropdown
        $(this.button).on('click', function(ev) {
            $(self.el).toggleClass('open');
        });

        // Bind onChange event
        $(this.form).on('change', function() {
            if (typeof self.onChange === 'function') {
                self.onChange.call(self, self.getValues(), self.getValuesSerialized());
            }
        });

        // Click clear button
        $(this.el).find('.clear-all').click(function() {
            self.clearAll();
            if (typeof self.onChange === 'function') {
                self.onChange.call(self, self.getValues(), self.getValuesSerialized());
            }
        });

        // Check all
        $(this.el).find('.check-all').click(function() {
            $(self.form).find('input').attr('checked', '');
            if (typeof self.onChange === 'function') {
                self.onChange.call(self, self.getValues(), self.getValuesSerialized());
            }
        });

        //Nadia
        $(this.el).find('.close-btn').click(function() {
            self.clearAll();
            if (typeof self.onChange === 'function') {
                self.onChange.call(self, self.getValues(), self.getValuesSerialized());
            }
            self.closeDropdown();
        });
        //Nadia

        $(this.el).find('.ok-btn').click(function() {

            if (typeof self.onSubmit === 'function') {
                self.onSubmit.call(self, self.getValues(), self.getValuesSerialized());
            }
            self.closeDropdown();
        });

        $('body').click(function(ev) {
             //if ($(ev.target).closest('.search-by-specialty-wrapper').length === 0) {
               //  $(self.el).removeClass('open');
             //}
        });
    };

    this.render = function() {
        this.bindEvents();
        return self;
    };

    this.getValues = function() {
        var array = $(this.form).serializeArray();
        array.forEach(function(item) {
            item.type = 'specialty';
        });

        return array;
    };

    this.setValues = function(values, silent) {};

    this.clearAll = function() {
        $(self.form).find('input').attr('checked', false);
    };

    this.getValuesSerialized = function() {
        return $(this.form).serialize();
    };

    this.closeDropdown = function() {
        $(this.el).removeClass('open');
    };

    this.removeOption = function(tag) {
        var self = this;
        $(self.el).find('form input[value="' + tag.value + '"]').attr('checked', false);
    };

    this.render();

    return this;

};


/**
 * Type form in Events map.
 */
var TypeForm = function(options) {

    this.el = options.el;
    this.button = options.button;

    this.form = $(this.button).find('form');
    this.onSubmit = options.onSubmit;
    this.onChange = options.onChange;
    var self = this;
    this.bindEvents = function() {

        var self = this;

        $(this.button).on('click', function(ev) {
            $(self.el).toggleClass('open');
        });

        // Bind onChange event
        $(this.form).on('change', function() {
            if (typeof self.onChange === 'function') {
                self.onChange.call(self, self.getValues(), self.getValuesSerialized());
            }
        });

        // Click clear button
        $(this.el).find('.clear-all').click(function() {
            self.clearAll();
            if (typeof self.onChange === 'function') {
                self.onChange.call(self, self.getValues(), self.getValuesSerialized());
            }
        });

        // Check all
        $(this.el).find('.check-all').click(function() {
            $(self.form).find('input').attr('checked', '');
            if (typeof self.onChange === 'function') {
                self.onChange.call(self, self.getValues(), self.getValuesSerialized());
            }
        });

        //Nadia
        $(self.button).find('.close-btn').click(function() {
            self.clearAll();
            if (typeof self.onChange === 'function') {
                self.onChange.call(self, self.getValues(), self.getValuesSerialized());
            }
            self.closeDropdown();
        });

        //Nadia

        $(self.button).find('.ok-btn').click(function() {


            $(self.button.form).find('input').attr('checked', '');

            if (typeof self.onChange === 'function') {
                 console.log('Print this here');
                self.onChange.call(self, self.getValues(), self.getValuesSerialized());
            }
            self.closeDropdown();
        });


        $(this.el).find('.search-btn').click(function() {

            if (typeof self.onSubmit === 'function') {
                self.onSubmit.call(self, self.getValues(), self.getValuesSerialized());
            }
            self.closeDropdown();
        });

        $('body').click(function(ev) {
            if ($(ev.target).closest('.search-by-type').length === 0) {
                $(self.el).removeClass('open');
            }
        });
    };

    this.render = function() {
        this.bindEvents();
        return self;
    };

    this.getValues = function() {

        var array = $(this.form).serializeArray();
        array.forEach(function(item) {
            item.type = 'type';
        });

        return array;
    };

    this.clearAll = function() {
        $(self.form).find('input').attr('checked', false);
    };

    this.getValuesSerialized = function() {
        return $(this.form).serialize();
    };

    this.closeDropdown = function() {
        $(this.button).removeClass('open'); //Nadia
    };

    this.removeOption = function(tag) {
        var self = this;
        $(self.el).find('form input[value="' + tag.value + '"]').attr('checked', false);
    };

    this.render();

    return this;

};


/**
 * Location form in Events map.
 */
var LocationForm = function(options) {

    var self = this;
    this.el = options.el;
    this.button = options.button;

    this.form = $(this.button).find('form');
    this.onSubmit = options.onSubmit;
    this.onChange = options.onChange;
    this.removeOption = options.removeOption;

    this.bindEvents = function() {

        this.doneTyping('.location-city, .location-country', function() {
            if (typeof self.onChange === 'function') {
                self.onChange.call(self, self.getValues(), self.getValuesSerialized());
            }
        });

        // Click clear button
        $(this.el).find('.clear-all').click(function() {
            self.clearAll();
            if (typeof self.onChange === 'function') {
                self.onChange.call(self, self.getValues(), self.getValuesSerialized());
            }
        });

        // Check all
        $(this.el).find('.check-all').click(function() {
            $(self.form).find('input').attr('checked', '');
            if (typeof self.onChange === 'function') {
                self.onChange.call(self, self.getValues(), self.getValuesSerialized());
            }
        });


        //Nadia
        $(self.button).find('.close-btn').click(function() {
            self.clearAll();
            if (typeof self.onChange === 'function') {
                self.onChange.call(self, self.getValues(), self.getValuesSerialized());
            }
            self.closeDropdown();
        });

        //Nadia
        $(self.button).find('.ok-btn').click(function() {

            $(self.button.form).find('input').attr('checked', '');

            if (typeof self.onChange === 'function') {
                console.log('Ok Submit');
                self.onChange.call(self, self.getValues(), self.getValuesSerialized());
            }
            self.closeDropdown();

        });


        $(this.el).find('.search-btn').click(function() {

            if (typeof self.onSubmit === 'function') {
                self.onSubmit.call(self, self.getValues(), self.getValuesSerialized());
            }
            self.closeDropdown();
        });

        $('body').click(function(ev) {
            if ($(ev.target).closest('.search-by-location').length === 0) {
                $(self.el).removeClass('open');
            }
        });
    };

    this.render = function() {
        this.bindEvents();
        return self;
    };

    this.getValues = function() {
        var values = [];
        $(self.button).find('input').each(function() {
            if ($(this).val() && $(this).val() !== '') {
                values.push({
                    value: $(this).val(),
                    type: 'location',
                    name: $(this).val()
                });
            }
        });
        return values;
    };

    this.clearAll = function() {
        $(self.form).find('input').val('');
    };

    this.getValuesSerialized = function() {
        return $(this.form).serialize();
    };

    this.closeDropdown = function() {
        $(this.button).removeClass('open');
    };

    this.removeOption = function(tag) {
        $(self.el).find('form input').each(function() {
            if ($(this).val() === tag.value) {
                $(this).val('');
            }
        });
    };

    this.doneTyping = function(input, cb) {
        //setup before functions
        var typingTimer; //timer identifier
        var doneTypingInterval = 200; //time in ms, 5 second for example
        var $input = $(input);

        //on keyup, start the countdown
        $input.on('keyup', function() {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(cb, doneTypingInterval);
        });

        //on keydown, clear the countdown
        $input.on('keydown', function() {
            clearTimeout(typingTimer);
        });
    };

    this.render();

    return this;

};

/**
 * Date form in Events map.
 */
var DateForm = function(options) {

    var self = this;
    this.el = options.el;
    this.button = options.button;

    this.form = $(this.button).find('form');

    this.onSubmit = options.onSubmit;
    this.onChange = options.onChange;
    this.removeOption = options.removeOption;

    this.bindEvents = function() {

        $(this.button).find('input').datepicker({ //here!
            onSelect: function(date) {
                if (typeof self.onChange === 'function') {
                    self.onChange.call(self, self.getValues(), self.getValuesSerialized());
                }
            }
        });

        // Click clear button
        $(this.el).find('.clear-all').click(function() {
            self.clearAll();
            if (typeof self.onChange === 'function') {
                self.onChange.call(self, self.getValues(), self.getValuesSerialized());
            }
        });

        // Check all
        $(this.el).find('.check-all').click(function() {
            $(self.form).find('input').attr('checked', '');
            if (typeof self.onChange === 'function') {
                self.onChange.call(self, self.getValues(), self.getValuesSerialized());
            }
        });

        //Nadia
        $(self.button).find('.close-btn').click(function() {
            self.clearAll();
            if (typeof self.onChange === 'function') {
                self.onChange.call(self, self.getValues(), self.getValuesSerialized());
            }
            self.closeDropdown();
        });

        //Nadia

        $(self.button).find('.ok-btn').click(function() {


            $(self.button.form).find('input').attr('checked', '');

            if (typeof self.onChange === 'function') {
                self.onChange.call(self, self.getValues(), self.getValuesSerialized());
            }
            self.closeDropdown();
        });


        $(this.el).find('.search-btn').click(function() {

            if (typeof self.onSubmit === 'function') {
                self.onSubmit.call(self, self.getValues(), self.getValuesSerialized());
            }
            self.closeDropdown();
        });

        $('body').click(function(ev) {
            if ($(ev.target).closest('.search-by-date').length === 0) {
                $(self.el).removeClass('open');
            }
        });

    };

    this.render = function() {
        this.bindEvents();
        return self;
    };

    this.getValues = function() {
        var values = [];
        $(self.button).find('input').each(function() {
            if ($(this).val() && $(this).val() !== '') {
                values.push({
                    value: $(this).val(),
                    type: 'date',
                    name: $(this).val()
                });
            }
        });
        return values;
    };

    this.clearAll = function() {
        $(self.form).find('input').val('');
    };

    this.getValuesSerialized = function() {
        return $(this.form).serialize();
    };

    this.closeDropdown = function() {
        $(this.button).removeClass('open');
    };

    this.removeOption = function(tag) {
        $(self.el).find('form input').each(function() {
            if ($(this).val() === tag.value) {
                $(this).val('');
            }
        });
    };

    this.doneTyping = function(input, cb) {
        //setup before functions
        var typingTimer; //timer identifier
        var doneTypingInterval = 200; //time in ms, 5 second for example
        var $input = $(input);

        //on keyup, start the countdown
        $input.on('keyup', function() {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(cb, doneTypingInterval);
        });

        //on keydown, clear the countdown
        $input.on('keydown', function() {
            clearTimeout(typingTimer);
        });
    };

    this.render();

    return this;

};


var SearchFilters = function(options) {

    this.el = options.el;
    this.onRemove = options.onRemove;
    this.eventsMap = options.eventsMap;
    this.filterViews = options.filterViews;
    this.searchResults = options.searchResults;

    this.values = {};
    var self = this;

    this.initialize = function() {
        this.render();
    };

    this.render = function() {
        $(this.el).tagit();
        this.bindEvents();
    };

    this.bindEvents = function() {
        $(self.el).tagit({
            afterTagRemoved: function(event, ui) {
                if (!self.removeSilent) {
                    var value;
                    $(ui.tag).attr('class').split(' ').forEach(function(className) {
                        if (className.indexOf('value') > -1) value = className.split('-')[1];
                    });
                    var type;
                    $(ui.tag).attr('class').split(' ').forEach(function(className) {
                        if (className.indexOf('type') > -1) type = className.split('-')[1];
                    });

                    var label = ui.tagLabel;
                    if (typeof self.onRemove === 'function') {
                        self.onRemove.call(self, {
                            value: value,
                            name: label,
                            type: type
                        });
                    }
                }
            }
        });
    };


    this.add = function(newTags) {
        newTags.forEach(function(tag) {
            $(self.el).tagit("createTag", tag.name, 'value-' + tag.value + ' type-' + tag.type);
        });
    };


    this.update = function(type, values, cb) {
        self.removeSilent = true;
        self.removeAll();
        self.removeSilent = false;
        self.values[type] = values;

        Object.keys(this.values).forEach(function(key) {
            self.add(self.values[key]);
        });

        self.fetch({
            addToMap: true,
            query: self.values
        }, function(events) {
            if (cb) cb(events);
        });

    };

    this.fetch = function(options, cb) {
        self.eventsMap.fetchEvents(options, function(events) {
            self.searchResults.render(events);
            if (cb) cb(events);
        });
    };



    this.removeAll = function() {
        $(self.el).tagit("removeAll");
    };

    this.initialize();
};


var SearchFiltersResults = function(options) {
    var self = this;
    this.el = options.el;
    self.events = [];

    this.bindEvents = function() {
        $('.see-more').click(function() {
            self.render(self.events, 'all');
        });
    };

    this.hide = function() {
        $('.map-search-results-wrapper').addClass('hidden');
    };

    this.render = function(events, all) {
        self.events = events;
        var $content = $(self.el).find('.content');
        $content.empty();
        if (events.length === 0) {
            self.hide();
        } else {
            var limit = (all) ? events.length : 9;
            var i = 0;
            $('.map-search-results-wrapper').removeClass('hidden');
            events.forEach(function(ev) {
                var types = (ev.types) ? '<div class="label-white-inner">' + ev.types + '</div>' : '';
                var innerHtml = '<div class="featured-event"><div class="label-white">' + types + '</div><div class="title">' + ev.title + '</div><div class="venue">' + ev.venue + '</div><div class="location-info"><span class="event-city">' + ev.city + ', ' + ev.nation + '</span> | ' + ev.date_from + '</div>';
                var html = '<div class="map-search-result-item col-md-4"><div class="featured-event">' + innerHtml + '</div></div>';
                if (i < limit) $content.append(html);
                i++;
            });
            var action = (all) ? 'hide' : 'show';
            $('.see-more')[action]();
        }
    };

    $('.map-search-results-wrapper').addClass('hidden');
    self.bindEvents();
};


$(document).ready(function() {

    //Nadia
    $('.map-search-submit').click(function() {

        $('.map-search-results-wrapper').show();
        $('.map-cta').hide();
    });

    // Remove map-cta on focus-out click
    $('.map-section').click(function(ev) {
        if ($(ev.target).closest('.map-cta').length === 0) {
            //$('.map-cta').hide();
        }
    });
    $('.map-section').append('<div class="map-zoom-out hidden" style="position:absolute;bottom:140px;left:50%;margin-left:-20px"><a class="btn btn-default btn-md">Zoom out</a></div>');

    // Bind events in dropdowns
    $('.dropd').click(function() {
        var target = $(this).data('target');
        $(target).toggleClass('open');
    });

    var eventsMap = new EventsMap({
        el: $('.map-canvas-wrapper')[0],
        wrapper: $('.map-section')[0],
        onBoundsChange: function(map) {

            var zoom = map.getZoom();
            if (zoom > 3) {
                $('.map-zoom-out').removeClass('hidden');
            } else {
                $('.map-zoom-out').addClass('hidden');
            }
        }
    });

    $('.map-zoom-out').click(function(){
        eventsMap.map.setZoom(3);
        eventsMap.map.setCenter(new google.maps.LatLng(40.6700, -73.9400));
    });

    // When the window has finished loading create our google map below
    google.maps.event.addDomListener(window, 'load', function() {
        // Initialize map
        eventsMap.init('.map-canvas-wrapper');
        $('.map-search-results-wrapper').hide();
        searchFilters.fetch({
            addToMap: true
        }, function(events) {});
    });

    var searchFilters = new SearchFilters({
        eventsMap: eventsMap,
        searchResults: new SearchFiltersResults({
            el: $('.map-search-results-inner')[0]
        }),
        el: $('.tagit')[0],
        filterViews: {
            specialty: new SpecialtyForm({
                el: $('.search-by-specialty-dropdown')[0],
                button: $('.search-by-specialty-inner')[0],
                onChange: function(values, serialized) {
                    // $('.map-search-results-wrapper').hide();
                     searchFilters.update('specialty', values, function() {
                         //$('.map-search-results-wrapper').show();
                     });
                },
                onSubmit: function(values, serialized) {
                    $('.map-search-results-wrapper').hide();
                    searchFilters.update('specialty', values, function() {
                         //$('.map-search-results-wrapper').show();
                    });
                }
            }),
            type: new TypeForm({
                el: $('.search-by-type')[0],
                button: $('.dropd-type')[0],
                onChange: function(values, serialized) {
                    $('.map-search-results-wrapper').hide();
                    searchFilters.update('type', values, function() {
                        // $('.map-search-results-wrapper').show();
                    });
                },
                onSubmit: function(values, serialized) {}
            }),
            location: new LocationForm({
                el: $('.search-by-location')[0],
                button: $('.dropd-location')[0],
                onChange: function(values, serialized) {
                    $('.map-search-results-wrapper').hide();
                    searchFilters.update('location', values, function() {
                       //  $('.map-search-results-wrapper').show();
                    });
                },
                onSubmit: function(values, serialized) {}
            }),
            date: new DateForm({
                el: $('.search-by-date')[0],
                button: $('.dropd-date')[0],
                onChange: function(values, serialized) {
                    $('.map-search-results-wrapper').hide();
                    searchFilters.update('date', values, function() {
                      //   $('.map-search-results-wrapper').show();
                    });
                },
                onSubmit: function(values, serialized) {}
            })
        },
        onRemove: function(tag) {
            this.filterViews[tag.type].removeOption(tag);
            this.values[tag.type] = this.filterViews[tag.type].getValues();
            $('.map-search-results-wrapper').hide();
            searchFilters.fetch({
                addToMap: true
            }, function(events) {
                // $('.map-search-results-wrapper').show();
            });
        }
    });


});
