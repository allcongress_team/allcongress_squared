
var stickyMenu = function(element) {
    var height=$(element);
    if (height.length>=1) {
        height=$(element).offset().top;
    }
    if (height.length) {
        $(window).scroll(function(ev){
            if($(window).scrollTop() >= height){
                $(element).addClass("fixed-menu");
            }
         else{
             $(element).removeClass("fixed-menu");
            }
        });
    }
};

// var scrollspy=function(name){
//     $(window).scroll(function(){
//         var selector='#'+name;
//         /*size of menu is 50 pixels so remove them from the previous section */
//         if($(window).scrollTop() >= $(selector).offset().top-50 &&
//            $(window).scrollTop() <= $(selector).offset().top+$(selector).height()-50) {
//                 $('a[data-href="'+name+'"]').parent().addClass("active");
//         }
//         else{
//             $('a[data-href="'+name+'"]').parent().removeClass("active");
//         }
//     });
// };

var scrollToAnimate=function(element){
    $('a[data-href="'+element+'"]').parent().click(function(){
        /*size of menu is 50 pixes  */
        $('html, body').animate({scrollTop: $('#'+element).offset().top-50}, 1000);
    });
};

var synchronized_carousel=function(){
    var sync1 = $("#sync1");
    var sync2 = $("#sync2");

    sync1.owlCarousel({
        singleItem : true,
        slideSpeed : 1000,
        navigation: true,
        navigationText: [
            "<i class='fa-chevron-left fa'></i>",
            "<i class='fa-chevron-right fa'></i>"
        ],
        pagination:false,
        afterAction : syncPosition,
        responsiveRefreshRate : 200


    });

    sync2.owlCarousel({
        items : 3,
        itemsDesktop      : [1199,3],
        itemsDesktopSmall     : [979,3],
        itemsTablet       : [768,3],
        itemsMobile       : [479,1],
        navigation: false,
        margin:10,
        stagePadding: 50,
        pagination:false,
        responsiveRefreshRate : 100,
        afterInit : function(el){
            el.find(".owl-item").eq(0).addClass("synced");
        }
    });

    function syncPosition(el){
        var current = this.currentItem;
        $("#sync2")
            .find(".owl-item")
            .removeClass("synced")
            .eq(current)
            .addClass("synced");
        if($("#sync2").data("owlCarousel") !== undefined){
            center(current);
        }
    }

    $("#sync2").on("click", ".owl-item", function(e){
        e.preventDefault();
        var number = $(this).data("owlItem");
        sync1.trigger("owl.goTo",number);
    });

    function center(number){
        var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
        var num = number;
        var found = false;
        for(var i in sync2visible){
            if(num === sync2visible[i]){
                var found = true;
            }
        }

        if(found===false){
            if(num>sync2visible[sync2visible.length-1]){
                sync2.trigger("owl.goTo", num - sync2visible.length+2);
            }else{
                if(num - 1 === -1){
                    num = 0;
                }
                sync2.trigger("owl.goTo", num);
            }
        } else if(num === sync2visible[sync2visible.length-1]){
            sync2.trigger("owl.goTo", sync2visible[1]);
        } else if(num === sync2visible[0]){
            sync2.trigger("owl.goTo", num-1);
        }

    }
};

$(window).load(function(){
    var height=$('#sync2 .owl-item').height()+'px';
    $('#sync1 .owl-prev').height( height ).css('bottom',-height);
    $('#sync1 .owl-next').height( height ).css('bottom',-height);
    //$('#sync1 i').css('line-height',height);
});



(function($) {



    $(document).ready(function() {
        /*
        *
        *
        *    maps at event page
        *
        *
        *
        *
        * */
      if(typeof $('#eventpage-map').data('coordinates') !== "undefined") { //Added this cause it was causing an error
        var coord=$('#eventpage-map').data('coordinates');
        var lat=parseFloat(coord.split(',')[0]);
        var lng=parseFloat(coord.split(',')[1]);
        var mapOptions = {
            // How zoomed in you want the map to start at (always required)
            zoom: 8,

            // The latitude and longitude to center the map (always required)
            center: new google.maps.LatLng(lat,lng), // New York

            // How you would like to style the map.
            // This is where you would paste any style found on Snazzy Maps.
            styles: [{
                "featureType": "landscape",
                "elementType": "labels",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "transit",
                "elementType": "labels",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "poi",
                "elementType": "labels",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "water",
                "elementType": "labels",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "stylers": [{
                    "hue": "#00aaff"
                }, {
                    "saturation": -100
                }, {
                    "gamma": 2.15
                }, {
                    "lightness": 12
                }]
            }, {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "lightness": 24
                }]
            }, {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [{
                    "lightness": 57
                }]
            }],
            scrollwheel: false
        };


        // When the window has finished loading create our google map below
        google.maps.event.addDomListener(window, 'load', function() {
            // Initialize map

        });
        // Get the HTML DOM element that will contain your map
        // We are using a div with id="map" seen below in the <body>
        var mapElement = $('#eventpage-map')[0];

        // Create the Google Map using our element and options defined above
        var map = new google.maps.Map(mapElement, mapOptions);



        //var bounds = new google.maps.LatLngBounds();


        var latlng = new google.maps.LatLng(lat,lng);
        var icon = '/sites/all/themes/allcongress_squared/assets/images/blue-marker.png';

        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            // animation: google.maps.Animation.DROP,
            title: "",
            icon: icon,
        });

        //extend the bounds to include each marker's position
        //bounds.extend(marker.position);

	  }

        /*

        events-page

        */
        synchronized_carousel();





    	$(".sidenav-button").sideNav();

        stickyMenu('#events-navbar');

        /* bootstrap scrollspy-for menu at events page */
        // scrollspy('event-overview');
        // scrollspy('event-general-info');
        // scrollspy('event-registration');
        // scrollspy('event-scientific-content');
        // scrollspy('event-contact');
        // scrollspy('event-map');
        // scrollspy('event-sponsors-exhibitor');
        // scrollspy('event-relevant-events');

        // scrollToAnimate('event-overview');
        // scrollToAnimate('event-general-info');
        // scrollToAnimate('event-registration');
        // scrollToAnimate('event-scientific-content');
        // scrollToAnimate('event-contact');
        // scrollToAnimate('event-map');
        // scrollToAnimate('event-sponsors-exhibitor');
        // scrollToAnimate('event-relevant-events');




        /*fix height of buttons in carousel */
        $(window).resize(function(){
            clearTimeout(window.resizedFinished);
            window.resizedFinished = setTimeout(function(){

                var height=$('#sync2 .owl-item').height()+'px';
                $('#sync1 .owl-prev').height( height ).css('bottom',-height);
                $('#sync1 .owl-next').height( height ).css('bottom',-height);
                $('#sync1 i').css('line-height',height);



            }, 250);

        });



        /*Owl carousel for frontpage featured events */
    // $(' .featured-events-wrapper .view-featured-events .view-content').owlCarousel({
    //   items : 3, //10 items above 1000px browser width
    //   itemsDesktop : [1000,3], //5 items between 1000px and 901px
    //   itemsDesktopSmall : [900,3], // betweem 900px and 601px
    //   itemsTablet: [600,2], //2 items between 600 and 0
    //   itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
    //      });

    });


})(jQuery);
