// Class.totop
jQuery(document).ready(function(){

	jQuery('.totop').hide();
});

jQuery(function() {
	jQuery(window).scroll(function() {
		if(jQuery(this).scrollTop() >= 200) {
			jQuery('.totop').fadeIn();
		} else {
			jQuery('.totop').fadeOut();
		}
	});

	jQuery('.totop').click(function() {
		jQuery('body,html').animate({scrollTop:0},800);
	});
});
