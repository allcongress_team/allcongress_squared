 $(document).ready(function() {


      	/*Owl carousel for frontpage featured events */
    	$(" .view.view-featured-events.view-id-featured_events .view-content").owlCarousel({
     		 items : 3, //10 items above 1000px browser width
     		 itemsDesktop : [1000,3], //3 items between 1000px and 901px
     		 itemsTablet: [768,2], //2 items between 768 and 480
      		 itemsMobile : [479,1] // itemsMobile 1
         });


        /*Owl carousel for frontpage testimonials */

       $(" .in-other-words .testimonials-list").owlCarousel({
          singleItem:true
       });

      //Show all medical news in homepage

       $('.medical-news-wrapper .more-link').click(function() {
            $('.medical-news-wrapper .view.view-view-medical-news ').addClass('show-all-med-news');
             $('.medical-news-wrapper .more-link').hide();

      });

       //Toggle between list view and grid view in search venues page
       $('.list-view-selector').click(function() {
          $('.list-view-selector').addClass('active');
          $('.grid-view-selector').removeClass('active');

          $('.view-venues-by-alphabetical-activeevents-popularity .view-content div .col-md-6.visible-md').addClass('hide'); //hide grid-view with display:none !important;
          $('.view-venues-by-alphabetical-activeevents-popularity .list-view').show();


      });

    $('.grid-view-selector').click(function() {
        $('.list-view-selector').removeClass('active');
        $('.grid-view-selector').addClass('active');

        $('.view-venues-by-alphabetical-activeevents-popularity .list-view').hide();
        $('.view-venues-by-alphabetical-activeevents-popularity .view-content div .col-md-6.visible-md').removeClass('hide'); //Show grid-view


    });


    // Make label orange when clicking on Search filter checkbox - Search pages
      $('.dropd-item  input[type="checkbox"]').on('change', function() {
        $(this).closest('label').toggleClass('checked');
      });

     // If cancel button is pressed, remove 'checked' class from labels
     $('.form-actions-wrapper .close-btn').click(function() {
            $('.dropd-item label').removeClass('checked');
     });

});
