# Theme allcongress_squared
* version: 0.0.1

## Software used

* git
* node.js
* npm
* grunt 0.4.5
* sass 3.4.13


## Startup guide

* Clone this repo in the desired directory

```
git clone https://vgiannoul@bitbucket.org/allcongress_team/allcongress_squared.git
```

* Enter the directory

```
$ cd allcongress_squared
```

* Checkout the desired branch e.g. master

```
$ git checkout master
```

* Install Grunt related stuff as a super user. You might need to change "sudo" according to your OS.

```
npm install
```

* Install gulp

```
npm install -g gulp
```

## Notes


### BEM Methodology example
https://github.com/drupalcampgr/landing2016

### Sass Directory structure
https://medium.com/@andersonorui_/bem-sass-and-bootstrap-9f89dc07d20f#.besgq2utv

### Bootstrap

http://www.sitepoint.com/5-useful-sass-mixins-bootstrap/
http://designmodo.com/bootstrap-css-part1/

### Style Sass files & csscomp

http://www.sitepoint.com/css-sass-styleguide/

### Gulp file structure

https://github.com/rasenplanscher/gulp-file-structure
https://github.com/Matthieusch/front-end/tree/master/gulp/tasks
https://github.com/chriskjaer/prototype-seed
